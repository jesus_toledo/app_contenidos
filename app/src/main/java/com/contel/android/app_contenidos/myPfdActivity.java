package com.contel.android.app_contenidos;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.contel.android.app_contenidos.Entidades.Contenido;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
public class myPfdActivity extends AppCompatActivity {
    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";
    private static final String EXTRA_SOCIAL_SECURITY_N = "com.contel.android.app_contenidos.StrSsn";
    private static final String EXTRA_CURSO_USUARIO_ID = "com.contel.android.app_contenidos.CursoUsuarioId";
    private static final int SWIPE_MIN_DISTANCE = 10;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final int MIN_SWIPPING_DISTANCE = 50;
    private static final int THRESHOLD_VELOCITY = 50;
    private ImageView img;
    private Bitmap mBitmap;
    private ImageView mImageView;//com.contel.android.app_contenidos.Gesture.GestureImageView mImageView;
    private int currentpage = -1;
    private Button mButtonNext, mButtonPrevious;
    private View mProgressView;
    private TextView mLblCurrentPage;
    private PdfRenderer.Page mCurrentPage;
    private ConsultaRutaPdfTask mConsultarRutaPdf;
    private static String FILENAME = "ejemplo1.pdf";
    private ParcelFileDescriptor mFileDescriptor;
    private PdfRenderer mPdfRenderer;
    private int mPageIndex;
    private String mContenidoId;
    private Contenido mContenido;
    //private GestureDetector gestureDetector;
    private GestureDetector gdt;
    View.OnTouchListener gestureListener;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pfd);

        mImageView = findViewById(R.id.image_pdf_viewer);//(com.contel.android.app_contenidos.Gesture.GestureImageView) findViewById(R.id.image_pdf_viewer);
        mButtonPrevious = findViewById(R.id.previous);
        mButtonNext = findViewById(R.id.next);
        mLblCurrentPage = findViewById(R.id.lbl_current_page);
        mProgressView = findViewById(R.id.pdf_download_progress);
        mPageIndex = 0;
        receiveData();
        // If there is a savedInstanceState (screen orientations, etc.), we restore the page index.
        if (null != savedInstanceState) {
            mPageIndex = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 0);
        }

        mButtonPrevious.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                //receiveData();
                showPage(mCurrentPage.getIndex() - 1);
            }
        });
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {

                //receiveData();
                showPage(mCurrentPage.getIndex() + 1);
            }
        });
        gdt = new GestureDetector(new GestureListener());
        mImageView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
                gdt.onTouchEvent(event);
                return true;
            } });
    }
    private void receiveData() {
        //RECEIVE DATA VIA INTENT
        Intent i = getIntent();
        mContenidoId = i.getStringExtra("CONTENIDO_ID");
        mContenido = ContenidoLab.get(this,"0").getContenido(mContenidoId);
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStart() {
        super.onStart();
        Descargar_Contenido();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStop() {
        try {
            closeRenderer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (null != mCurrentPage) {
            outState.putInt(STATE_CURRENT_PAGE_INDEX, mCurrentPage.getIndex());
        }
    }
    /*@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.previous: {
                // Move to the previous page
                showPage(mCurrentPage.getIndex() - 1);
                break;
            }
            case R.id.next: {
                // Move to the next page
                showPage(mCurrentPage.getIndex() + 1);
                break;
            }
        }
    }*/
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showPage(int index) {
        if(index<0)
            index=0;
        if (mPdfRenderer.getPageCount() <= index) {
            return;
        }
        // Make sure to close the current page before opening another one.
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        // Use `openPage` to open a specific page in PDF.
        mCurrentPage = mPdfRenderer.openPage(index);
        // Important: the destination bitmap must be ARGB (not RGB).
        mBitmap = Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(),
                Bitmap.Config.ARGB_8888);
        // Here, we render the page onto the Bitmap.
        // To render a portion of the page, use the second and third parameter. Pass nulls to get
        // the default result.
        // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
        mCurrentPage.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        // We are ready to show the Bitmap to user.
        //mImageView = new ImageView(getApplication());//new com.contel.android.app_contenidos.Gesture.GestureImageView(this);
        mImageView.setImageBitmap(mBitmap);
        //mImageView.reset(); //setImageBitmap(mBitmap);
        updateUi();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void updateUi() {
        String lbl_progress = getResources().getString(R.string.current_pdf_page);
        int index = mCurrentPage.getIndex();
        int pageCount = mPdfRenderer.getPageCount();
        mLblCurrentPage.setVisibility(View.VISIBLE);
        mButtonPrevious.setEnabled(0 != index);
        mButtonNext.setEnabled(index + 1 < pageCount);
        lbl_progress = lbl_progress.replace("#",String.valueOf(index+1));
        lbl_progress = lbl_progress.replace("$",String.valueOf(pageCount));
        mLblCurrentPage.setText(lbl_progress);
    }
    BroadcastReceiver onComplete=new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void onReceive(Context ctxt, Intent intent) {
            Toast.makeText(myPfdActivity.this,"Se descargó PDF", Toast.LENGTH_SHORT).show();
            try {
                showProgress(false);
                openRenderer(myPfdActivity.this);
                showPage(mPageIndex);

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(myPfdActivity.this, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void Descargar_Contenido() {
        String[] Partes_Url;
        String url = mContenido.getUrl_acceso();

        String externalStroageString = String.valueOf( getFilesDir() );//+"/Pdf/";
        Partes_Url = url.split("/");
        String fileName="/" + Partes_Url[Partes_Url.length-1];
        FILENAME = fileName;
        //File file = new File(externalStroageString);
        File file = new File(Environment.getExternalStoragePublicDirectory(String.valueOf(getFilesDir()+"/")), fileName);
        File dir = new File (externalStroageString);
        if(dir.exists()==false) {
            dir.mkdirs();
        }
        if (file.isFile()==false) {
            showProgress(true);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            registerReceiver(onComplete,new IntentFilter( DownloadManager.ACTION_DOWNLOAD_COMPLETE ));
            request.setDescription(FILENAME);
            request.setTitle("Contenidos App");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(externalStroageString , fileName); //Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }
        else{ try {
            openRenderer(this);
            showPage(mPageIndex);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } }
    }
    public static Intent newIntent(Context packageContext, String StrSsn) {
        Intent intent = new Intent(packageContext, myPfdActivity.class);
        intent.putExtra(EXTRA_SOCIAL_SECURITY_N, StrSsn);
        return intent;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openRenderer(Context context) throws IOException {

        // In this sample, we read a PDF from the assets directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(String.valueOf(getFilesDir()+"/")), FILENAME);

        if (!file.exists()) {

            // Since PdfRenderer cannot handle the compressed asset file directly, we copy it into

            // the cache directory.

            InputStream asset = context.getAssets().open(FILENAME);

            FileOutputStream output = new FileOutputStream(file);

            final byte[] buffer = new byte[1024];

            int size;

            while ((size = asset.read(buffer)) != -1) {

                output.write(buffer, 0, size);

            }
            asset.close();
            output.close();
        }

        mFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

        // This is the PdfRenderer we use to render the PDF.

        if (mFileDescriptor != null) {

            mPdfRenderer = new PdfRenderer(mFileDescriptor);

        }

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void closeRenderer() throws IOException {
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        mPdfRenderer.close();
        mFileDescriptor.close();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getPageCount() {

        return mPdfRenderer.getPageCount();

    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            if (e1.getX() - e2.getX() > MIN_SWIPPING_DISTANCE && Math.abs(velocityX) > THRESHOLD_VELOCITY)
            {
                showPage(mCurrentPage.getIndex() + 1);

           /* Code that you want to do on swiping left side*/
                return false;
            }
            else if (e2.getX() - e1.getX() > MIN_SWIPPING_DISTANCE && Math.abs(velocityX) > THRESHOLD_VELOCITY)
            {
                showPage(mCurrentPage.getIndex() - 1);

          /* Code that you want to do on swiping right side*/
                return false;
            }
            return false;
        }
    }
    public class ConsultaRutaPdfTask extends AsyncTask<Void,Void,Boolean> {
        private final String mId;
        private final String mLat;
        private final String mLong;
        private String IdRespuesta;
        ConsultaRutaPdfTask(String Id, String Lat, String Long) {
            mId = Id;
            mLat = Lat;
            mLong = Long;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                new HttpContenidos().EnviarGeoLocalizacion(mId,mLat,mLong);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        protected void onPostExecute(final Boolean success){
            mConsultarRutaPdf = null;
            showProgress(false);
            if (success) {

            }
            else {

            }
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        protected void onCancelled() {
            mConsultarRutaPdf = null;
            showProgress(false);
        }
    }
}
