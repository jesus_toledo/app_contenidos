package com.contel.android.app_contenidos;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.contel.android.app_contenidos.Camara.CameraConfig;
import com.contel.android.app_contenidos.Camara.CameraPreview;
import com.contel.android.app_contenidos.Camara.GeoBioService;
import com.contel.android.app_contenidos.Camara.HiddenCameraFragment;
import com.contel.android.app_contenidos.Entidades.Cuestionario;
import com.contel.android.app_contenidos.Entidades.Respuestas;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;
import com.contel.android.app_contenidos.Servicios.ClienteContenido;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class QuestionaryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String KEY_LISTA_RESPUESTAS = "lista_respuestas";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button mBtnEnviar;

    private OnFragmentInteractionListener mListener;
    private View mView;
    private String mContenidoId;
    private String mCursoUsuarioId;
    private String mCuestionarioId;
    private int mContadorPausasQuestionario;
    private int mContadorPausasQuestionario_delay;
    private String mMinutoPausa;
    private View mHomeFormView;
    private View mProgressView;
    private Button mBtn_Enviar;
    private ImageButton mBtn_Siguiente_Pregunta;
    private ImageButton mBtn_Anterior_Pregunta;
    private int mCurrentIndex = 0;
    private String mTipoContenido = "0";
    private String mContadorPausasIntent = "0";
    private TextView mLbl_total_preguntas;
    private TextView mPregunta;
    private RadioButton mRespuesta_1;
    private RadioButton mRespuesta_2;
    private RadioButton mRespuesta_3;
    private RadioButton mRespuesta_4;
    private RadioButton mRespuesta_5;
    private RadioGroup mRadioGroup;
    private ObtenerCuestionarioTask mPreguntasTask = null;
    private EnviaGPSTask mGpsTask = null;
    private EnviarCuestionarioTask mEnviarRespuestasTask = null;
    private List<Cuestionario> mCuestionario;
    private List<Respuestas> mRespuestas;
    private List<Respuestas> mRespuestasPreguntaCurso;
    private String ID_Respuestas;
    private boolean mTomo_Foto = false;
    //Location
    private LocationManager mLocationManager;
    private double mLongitudeBest;
    private double mLatitudeBest;
    private static final String TAG = "ContenidoFragment";
    //Camara
    private HiddenCameraFragment mHiddenCameraFragment;
    private CameraPreview mCameraPreview;
    private CameraConfig mCachedCameraConfig;

    public QuestionaryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QuestionaryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuestionaryFragment newInstance(String param1, String param2) {
        QuestionaryFragment fragment = new QuestionaryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK)
            Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.lbl_send_validation),
                    Toast.LENGTH_SHORT).show();

        return false;
        // Disable back button..............
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_questionary, container, false);
        receiveData();
        mBtnEnviar = mView.findViewById(R.id.btn_enviar_preguntas);
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Activity a = getActivity();
        if(a != null)
            a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mHomeFormView = mView.findViewById(R.id.quiz_form);
        mProgressView = mView.findViewById(R.id.progressbar_quiz);
        mBtn_Enviar = mView.findViewById(R.id.btn_enviar_preguntas);
        mBtn_Anterior_Pregunta= mView.findViewById(R.id.Btn_Anterior_Pregunta);
        mBtn_Siguiente_Pregunta = mView.findViewById(R.id.Btn_Siguiente_Pregunta);
        mPregunta = mView.findViewById(R.id.txt_pregunta);
        mRespuesta_1 = mView.findViewById(R.id.rbt_respuesta_1);
        mRespuesta_2 = mView.findViewById(R.id.rbt_respuesta_2);
        mRespuesta_3 = mView.findViewById(R.id.rbt_respuesta_3);
        mRespuesta_4 = mView.findViewById(R.id.rbt_respuesta_4);
        mRespuesta_5 = mView.findViewById(R.id.rbt_respuesta_5);
        mRadioGroup = mView.findViewById(R.id.rdg_respuestas);
        mLbl_total_preguntas = mView.findViewById(R.id.lbl_total_preguntas);
        mPregunta.setText(R.string.lorem_ipsum);
        mRespuesta_1.setText("Respuesta 1");
        mRespuesta_2.setText("Respuesta 2");
        mRespuesta_3.setText("Respuesta 3");
        mRespuesta_4.setText("Respuesta 4");
        mRespuesta_5.setText("Respuesta 5");
        showProgress(true);
        //receiveData();
        toggleNetworkUpdates();
        mPreguntasTask = new ObtenerCuestionarioTask(mContenidoId,mMinutoPausa);
        mPreguntasTask.execute((Void) null);
        //Captura_Bio_Geo_Referencia();
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radioButton = mRadioGroup.findViewById(checkedId);
                int index = mRadioGroup.indexOfChild(radioButton);
                RadioButton r = (RadioButton)  mRadioGroup.getChildAt(index);
                Actualizar_Respuestas(index);
            }
        });

        mBtnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btn_Enviar_Click();
            }
        });
        mBtn_Siguiente_Pregunta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Siguiente_Pregunta();
            }
        });
        mBtn_Anterior_Pregunta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Anterior_Pregunta();
            }
        });
        return  mView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void Captura_Bio_Geo_Referencia() {
        if (mHiddenCameraFragment != null) {    //Remove fragment from container if present
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .remove(mHiddenCameraFragment)
                    .commit();
            mHiddenCameraFragment = null;
        }

        //startService(new Intent(QuestionarioActivity.this, GeoBioService.class));

        Intent serviceIntent = new Intent(getActivity(),GeoBioService.class);
        serviceIntent.putExtra("Contenido_Id", String.valueOf(mContenidoId));
        serviceIntent.putExtra("Curso_Usuario_Id", String.valueOf(mCursoUsuarioId));
        serviceIntent.putExtra("GSPLatitud", String.valueOf(mLatitudeBest));
        serviceIntent.putExtra("GPSLongitud", String.valueOf(mLongitudeBest));
        Log.i(TAG,"varaibles para envio: Contenidoid=" + mContenidoId + "curso_id=" + mCursoUsuarioId + "latitud=" + mLatitudeBest + " longitud=" + mLongitudeBest);
        getActivity().startService(serviceIntent);
        mLocationManager.removeUpdates(locationListenerBest);
    }
    private void Actualizar_Respuestas(int indice) {
        String idPregunta;
        String idRespuesta;
        if (mRespuestasPreguntaCurso.size() > 0) {
            idPregunta = mRespuestasPreguntaCurso.get(0).getPregunta_id();
            idRespuesta = mRespuestasPreguntaCurso.get(indice).getRespuesta_id();
            for (Cuestionario c : mCuestionario) {
                if (c.getPregunta_id().equals(idPregunta)) {
                    c.setRespuesta_id(idRespuesta);
                }
            }
        }
    }

    public void takePicture() {
        if (mCameraPreview != null) {
            if (mCameraPreview.isSafeToTakePictureInternal()) {
                mCameraPreview.takePictureInternal();
            }
        } else {
            throw new RuntimeException("Background camera not initialized. Call startCamera() to initialize the camera.");
        }
    }
    public void Enviar_Preguntas(View view){
        Button botonActual = (Button) view;
        Intent i = new Intent(getActivity(),
                myVideoActivity.class);
        i.putExtra("CONTENIDO_ID", "1");
        i.putExtra("MINUTO_VIDEO", 6001);
        i.putExtra("CONTADOR_PAUSAS", 2);
        //START ACTIVITY
        startActivity(i);
    }
    View.OnClickListener first_radio_listener = new View.OnClickListener(){
        public void onClick(View v) {
            //Your Implementaions...
        }
    };


    public class ObtenerCuestionarioTask extends AsyncTask<Void,Void,Boolean> {
        private final String mContenidoDetalleId;
        private final String mMilisegundo;
        ObtenerCuestionarioTask(String contenido_detalle_id,String milisegundo) {
            mContenidoDetalleId = contenido_detalle_id;
            mMilisegundo = milisegundo;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                //if(!Conexion.isNetworkAvailableAndConnected(QuestionarioActivity.this) || !Conexion.isOnlineNet() )
                //{
                mCuestionarioId = new HttpContenidos().ObtenerIdCuestionario(mContenidoDetalleId,mMilisegundo,mTipoContenido);
                mCuestionario = new HttpContenidos().ObtenerCuestionarioPeticion(mContenidoDetalleId,mMilisegundo,mTipoContenido);
                mRespuestas = new HttpContenidos().ObtenerRespuestasPeticion(mContenidoDetalleId,mMilisegundo,mTipoContenido);
                //}
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mPreguntasTask = null;
            showProgress(false);
            if (success) {
                updateQuestion();
            }
            else {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getContext().getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        protected void onCancelled() {
            mPreguntasTask = null;
            showProgress(false);
        }
    }
    public class EnviaGPSTask extends AsyncTask<Void,Void,Boolean> {
        private final String mId;
        private final String mLat;
        private final String mLong;
        private String IdRespuesta;
        EnviaGPSTask(String Id, String Lat, String Long) {
            mId = Id;
            mLat = Lat;
            mLong = Long;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                new HttpContenidos().EnviarGeoLocalizacion(mId,mLat,mLong);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mPreguntasTask = null;
            showProgress(false);
            if (success) {
                Log.i(TAG,"Se envio a la base de datos la georeferencia y se procede a tomar foto");
                Captura_Bio_Geo_Referencia();
            }
            else {

            }
        }
        protected void onCancelled() {
            mPreguntasTask = null;
            showProgress(false);
        }
    }
    public class EnviarCuestionarioTask extends AsyncTask<Void,Void,Boolean> {
        private final String mRespuestas;
        private String IdRespuesta;
        EnviarCuestionarioTask(String Cadenajson_Respuestas) {
            mRespuestas = Cadenajson_Respuestas;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                IdRespuesta = new HttpContenidos().EnviarCuestionarioPeticion(mCursoUsuarioId,mContenidoId,mCuestionarioId, mRespuestas);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mPreguntasTask = null;
            showProgress(false);
            if (success) {
                mLocationManager.removeUpdates(locationListenerNetwork);
                ID_Respuestas = IdRespuesta;

                //Regresar
                if(mTipoContenido.equals("video"))
                {
                ContenidoFragment fr=new ContenidoFragment();
                Bundle bn = new Bundle();
                bn.putString("CONTENIDO_ID", mContenidoId);
                bn.putString("CURSO_USUARIO_ID", mCursoUsuarioId);
                bn.putInt("CONTADOR_PAUSAS", mContadorPausasQuestionario + 1);
                bn.putInt("CONTADOR_PAUSAS_DELAY", mContadorPausasQuestionario_delay);
                fr.setArguments(bn);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,fr)
                        .addToBackStack(null)
                        .commit();
                }
                else{
                    PdfFragment fr=new PdfFragment();
                    Bundle bn = new Bundle();
                    bn.putString("CONTENIDO_ID", mContenidoId);
                    bn.putString("CURSO_USUARIO_ID", mCursoUsuarioId);
                    bn.putInt("CONTADOR_PAUSAS", mContadorPausasQuestionario + 1);
                    bn.putInt("CONTADOR_PAUSAS_DELAY", mContadorPausasQuestionario_delay);
                    bn.putString("MINUTO_PAUSA", mMinutoPausa);
                    fr.setArguments(bn);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,fr)
                            .addToBackStack(null)
                            .commit();
                }

            }
            else {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity().getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        protected void onCancelled() {
            mPreguntasTask = null;
            showProgress(false);
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mHomeFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mBtn_Enviar.setVisibility(show ? View.GONE : View.VISIBLE);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mBtn_Enviar.setVisibility(show ? View.GONE : View.GONE);
        }
    }
    public void Siguiente_Pregunta(){
        mCurrentIndex ++;
        if(mCurrentIndex>=mCuestionario.size() )
            mCurrentIndex --;
        else
            updateQuestion();
    }
    public void Anterior_Pregunta(){
        mCurrentIndex --;
        if(mCurrentIndex<0 )
            mCurrentIndex ++;
        else
            updateQuestion();
    }
    private void updateQuestion(){
        String question = "";// = mCuestionario.get(mCurrentIndex).getNombre_pregunta();
        Cuestionario pregunta;
        String Respuesta_1 = "";
        String Respuesta_2 = "";
        String Respuesta_3 = "";
        String Respuesta_4 = "";
        String Respuesta_5 = "";
        String Respuesta_Seleccionada_ID = "";
        mRespuesta_3.setVisibility(View.VISIBLE);
        mRespuesta_4.setVisibility(View.VISIBLE);
        mRespuesta_5.setVisibility(View.VISIBLE);
        mRespuesta_2.setVisibility(View.VISIBLE);
        mRespuesta_1.setVisibility(View.VISIBLE);
        pregunta = mCuestionario.get(mCurrentIndex);
        question = pregunta.getNombre_pregunta();
        //mRespuestasPreguntaCurso = mRespuestas.stream().filter( respuestas -> respuestas.getPregunta_id().equals(pregunta.getPregunta_id()) ).collect(Collectors.toList());
        mLbl_total_preguntas.setText(mCurrentIndex + 1 + " de " + mCuestionario.size());
        mPregunta.setText(question);
        mRespuestasPreguntaCurso = new ArrayList<>();
        mRadioGroup.clearCheck();
        for(Respuestas r : mRespuestas){
            if(r.getPregunta_id().equals(pregunta.getPregunta_id()))
                mRespuestasPreguntaCurso.add(r);
        }
        switch( mRespuestasPreguntaCurso.size() ) {

            case 2:
                mRespuesta_3.setVisibility(View.INVISIBLE);
                mRespuesta_4.setVisibility(View.INVISIBLE);
                mRespuesta_5.setVisibility(View.INVISIBLE);
                break;
            case 3:
                mRespuesta_4.setVisibility(View.INVISIBLE);
                mRespuesta_5.setVisibility(View.INVISIBLE);
                break;
            case 4:
                mRespuesta_5.setVisibility(View.INVISIBLE);
                break;

        }
        Respuesta_Seleccionada_ID = pregunta.getRespuesta_id();
        int i = 1;
        for( Respuestas r : mRespuestasPreguntaCurso )
        {
            if(i==1)
            {
                mRespuesta_1.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_1.setChecked(true);
                    else
                        mRespuesta_1.setChecked(false);
                }
            }
            if(i==2){
                mRespuesta_2.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_2.setChecked(true);
                    else
                        mRespuesta_2.setChecked(false);
                }
            }
            if(i==3){
                mRespuesta_3.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_3.setChecked(true);
                    else
                        mRespuesta_3.setChecked(false);
                }
            }
            if(i==4){
                mRespuesta_4.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_4.setChecked(true);
                    else
                        mRespuesta_4.setChecked(false);
                }
            }
            if(i==5){
                mRespuesta_5.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_5.setChecked(true);
                    else
                        mRespuesta_5.setChecked(false);
                }
            }
            i++;
        }
    }
    public static boolean isNullOrBlank(String param) {
        return param == null || param.trim().length() == 0;
    }
    public  void Btn_Enviar_Click(){
        List<String> foo = new ArrayList<String>();
        for(Cuestionario c : mCuestionario){
            if(isNullOrBlank(c.getRespuesta_id())){
                showAlert(getResources().getString(R.string.lbl_send_quiz),getResources().getString(R.string.lbl_send_validation ));
                return;
            }
        }

        String json = new Gson().toJson(mCuestionario);
        showProgress(true);
        mEnviarRespuestasTask = new EnviarCuestionarioTask(json);
        mEnviarRespuestasTask.execute((Void) null);
    }
    private boolean isLocationEnabled(){
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    private void showAlert(String Title, String Mensaje){
        final AlertDialog.Builder mensajeAlerta = new AlertDialog.Builder(getActivity());
        mensajeAlerta.setTitle(Title).setMessage(Mensaje).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Intent myIntent= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //startActivity(myIntent);
            }
        });
        mensajeAlerta.show();
    }
    private void showAlert(){
        final AlertDialog.Builder mensajeAlerta = new AlertDialog.Builder(getActivity());
        mensajeAlerta.setTitle("Habilitar ubicación").setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                "para usar esta app").setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent myIntent= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        mensajeAlerta.show();
    }
    private boolean checkLocation(){
        if(!isLocationEnabled())
            showAlert();
        return  isLocationEnabled();
    }
    public void toggleNetworkUpdates(){
        if(!checkLocation())
            return;
            mLocationManager.removeUpdates(locationListenerNetwork);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,20*1000,10,locationListenerNetwork);
            Toast.makeText(getActivity(),"Network provider started running",Toast.LENGTH_SHORT).show();
        }
    }
        public void toggleBestUpdates(){
        if(!checkLocation())
            return;
        mLocationManager.removeUpdates(locationListenerBest);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            String provider = mLocationManager.getBestProvider(criteria,true);
            if(provider!=null){
                mLocationManager.requestLocationUpdates(provider,2*20*1000,10,locationListenerBest);
            }
        }
    }
    private final LocationListener locationListenerBest = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLongitudeBest = location.getLongitude();
            mLatitudeBest = location.getLatitude();
            mandarCoordenadas();
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            mLongitudeBest = location.getLongitude();
            mLatitudeBest = location.getLatitude();
            mandarCoordenadas();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private final LocationListener locationListenerNetwork = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLongitudeBest = location.getLongitude();
            mLatitudeBest = location.getLatitude();
            mandarCoordenadas();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    public  void  mandarCoordenadas(){


        ClienteContenido.getClienteContenido()
                .EnviarGeoLocalizacion(mContenidoId,String.valueOf(mLatitudeBest), String.valueOf(mLatitudeBest))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe: ");
                    }

                    @Override
                    public void onNext(String s) {

                        Log.d(TAG, "onNext: ");
                        Captura_Bio_Geo_Referencia();

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ");

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "Observable Completo: ");
                    }
                });

    }
    private void receiveData() {
        Bundle bundle=getArguments();
        mContenidoId = bundle.getString("CONTENIDO_ID");
        mCursoUsuarioId = bundle.getString("CURSO_USUARIO_ID");
        mContadorPausasQuestionario = bundle.getInt("CONTADOR_PAUSAS");
        mContadorPausasQuestionario_delay = bundle.getInt("CONTADOR_PAUSAS_DELAY");
        mMinutoPausa = bundle.getString("MINUTO_PAUSA");
        mTipoContenido = bundle.getString("TIPO_CONTENIDO");
    }
}
