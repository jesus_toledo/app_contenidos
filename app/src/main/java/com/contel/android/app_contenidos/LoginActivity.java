package com.contel.android.app_contenidos;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.contel.android.app_contenidos.Peticiones.HttpContenidos;
import com.contel.android.app_contenidos.Utils.MaskedWatcher;
import com.contel.android.app_contenidos.Validaciones.Conexion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity{
    private static final int REQUEST_LOCATION = 2;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static final String TAG = "loginActivity";
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    public String StrSsn;
    // UI references.
    private EditText mNssView;
    //private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    final String[] permisos = {
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_CONTACTS
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(!seTienenPermisos()) {
            ActivityCompat.requestPermissions(this, permisos, MY_CAMERA_REQUEST_CODE);
        }

        //--
//        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ){
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.CAMERA},
//                    MY_CAMERA_REQUEST_CODE);
//        }
//
//        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                    REQUEST_LOCATION);
//        }
//
//        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
//                    REQUEST_LOCATION);
//        }
//
//        int check = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//
//        if (check == PackageManager.PERMISSION_GRANTED) {
//            //Do something
//        } else {
//            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1024);
//        }
//        check = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
//        if (check == PackageManager.PERMISSION_GRANTED) {
//            //Do something
//        } else {
//            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1024);
//        }
//        check = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
//        if (check == PackageManager.PERMISSION_GRANTED) {
//            //Do something
//        } else {
//            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},1024);
//        }
        //--

        // Set up the login form.
        mNssView = findViewById(R.id.nss);

//        mPasswordView = (EditText) findViewById(R.id.password);
//        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
//                    attemptLogin();
//                    return true;
//                }
//                return false;
//            }
//        });

        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //InputMethodManager imm = (InputMethodManager) getSystemService(LoginActivity.INPUT_METHOD_SERVICE);
                //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                //if(!Conexion.isNetworkAvailableAndConnected(LoginActivity.this) || !Conexion.isOnlineNet() )
                    //showAlert("Contenidos",getResources().getString(R.string.connection_error));
                //else
                    attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mNssView.addTextChangedListener(
                new MaskedWatcher("###-##-####")
        );
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void showAlert(String Title, String Mensaje){
        final AlertDialog.Builder mensajeAlerta = new AlertDialog.Builder(LoginActivity.this);
        mensajeAlerta.setTitle(Title).setMessage(Mensaje).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Intent myIntent= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //startActivity(myIntent);
            }
        });
        mensajeAlerta.show();
    }
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mNssView.setError(null);
        //mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        StrSsn = mNssView.getText().toString().replace("-","");
//        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        /*if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }*/

        // Check for a valid email address.
        if (TextUtils.isEmpty(StrSsn)) {
            mNssView.setError(getString(R.string.error_field_required));
            focusView = mNssView;
            cancel = true;
        } else if (!isSsnValid(StrSsn)) {
            mNssView.setError(getString(R.string.error_invalid_ssn));
            focusView = mNssView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(StrSsn);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isSsnValid(String cadena) {
        //TODO: Replace this with your own logic
        if(cadena.indexOf("-")>-1)
                return cadena.length()==11;
            else
                return cadena.length()==9;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    boolean seTienenPermisos() {
        Boolean persmisosConcedidos = true;

        for (String permiso : permisos) {
            int estadoPermiso = ContextCompat.checkSelfPermission(this, permiso);
            if (estadoPermiso == PackageManager.PERMISSION_DENIED) {
                persmisosConcedidos = false;
                break;
            }
        }

        return persmisosConcedidos;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionsResult");
        switch (requestCode) {
            case MY_CAMERA_REQUEST_CODE:
                if (!seTienenPermisos()) {
                    //ActivityCompat.requestPermissions(this, permisos, MY_CAMERA_REQUEST_CODE);
                    AlertDialog.Builder notificacion = new AlertDialog.Builder(this);
                            notificacion.setTitle("Error")
                            .setMessage("Es necesario conceder todos los permisos.")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    LoginActivity.this.finish();
                                }
                            })
                            .create().show();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        Boolean blnAcceso = false;
        private final String mSsn;
        //private final String mPassword;

        UserLoginTask(String ssn) {
            mSsn = ssn;
            //mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(1);
                blnAcceso = new HttpContenidos().loginPeticion(mSsn);//
            } catch (InterruptedException e) {
                return false;
            }
            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            if (success) {
                if(blnAcceso) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.welcome_message, Toast.LENGTH_SHORT).show();
                            //Start HomeActivity
                            Intent i = HomeActivity.newIntent(LoginActivity.this, StrSsn);
                            startActivity(i);
                        }
                    });
                }
                else{
                    runOnUiThread(new Runnable() {

                        public void run() {

                            Toast.makeText(getApplicationContext(), R.string.not_welcome_message, Toast.LENGTH_LONG).show();

                        }
                    });
                }
                //finish();
            } else {
                mNssView.setError(getString(R.string.error_incorrect_nns));
                mNssView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

