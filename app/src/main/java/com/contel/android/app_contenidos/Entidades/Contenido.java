package com.contel.android.app_contenidos.Entidades;

import java.util.Date;
import java.util.UUID;

/**
 * Created by jtoledo on 18/04/2018.
 */

public class Contenido {
    private String contenido_id;
    private String nombre_contenido;
    private String total_duracion;
    private String total_paginas;
    private String url_acceso;
    private String estatus;
    private String extension_archivo;
    private String descripcion;
    private String streaming;

    public String getNombre_contenido() {
        return nombre_contenido;
    }
    public void setNombre_contenido(String nombre_contenido) { this.nombre_contenido = nombre_contenido;  }
    public String getTotal_duracion() { return total_duracion;  }
    public void setTotal_duracion(String total_duracion) {
        this.total_duracion = total_duracion;
    }
    public String getTotal_paginas() { return total_paginas;  }
    public void setTotal_paginas(String total_paginas) {
        this.total_paginas = total_paginas;
    }
    public String getUrl_acceso() {
        return url_acceso;
    }
    public void setUrl_acceso(String url_acceso) {
        this.url_acceso = url_acceso;
    }
    public String getEstatus() {
        return estatus;
    }
    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
    public String getExtension_archivo() {
        return extension_archivo;
    }
    public void setExtension_archivo(String extension_archivo) { this.extension_archivo = extension_archivo;  }
    public String getContenido_id() {
        return contenido_id;
    }
    public void setContenido_id(String contenido_id) {
        this.contenido_id = contenido_id;
    }
    public String getDescripcion() { return descripcion;  }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getStreaming() {
        return streaming;
    }

    public void setStreaming(String streaming) {
        this.streaming = streaming;
    }
}
