package com.contel.android.app_contenidos;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.contel.android.app_contenidos.Validaciones.Conexion;
import com.contel.android.app_contenidos.Camara.CameraConfig;
import com.contel.android.app_contenidos.Camara.CameraPreview;
import com.contel.android.app_contenidos.Camara.GeoBioService;
import com.contel.android.app_contenidos.Camara.HiddenCameraFragment;
import com.contel.android.app_contenidos.Entidades.Cuestionario;
import com.contel.android.app_contenidos.Entidades.Respuestas;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class QuestionarioActivity extends AppCompatActivity {
    private String mContenidoId;
    private String mCursoUsuarioId;
    private String mMinutoPausa;
    private View mHomeFormView;
    private View mProgressView;
    private Button mBtn_Enviar;
    private int
            mCurrentIndex = 0;
    private int mContadorPausasQuestionario = 0;
    private String mContadorPausasIntent = "0";
    private TextView mLbl_total_preguntas;
    private TextView mPregunta;
    private RadioButton mRespuesta_1;
    private RadioButton mRespuesta_2;
    private RadioButton mRespuesta_3;
    private RadioButton mRespuesta_4;
    private RadioButton mRespuesta_5;
    private RadioGroup mRadioGroup;
    private ObtenerCuestionarioTask mPreguntasTask = null;
    private EnviaGPSTask mGpsTask = null;
    private EnviarCuestionarioTask mEnviarRespuestasTask = null;
    private List<Cuestionario> mCuestionario;
    private List<Respuestas> mRespuestas;
    private List<Respuestas> mRespuestasPreguntaCurso;
    private String ID_Respuestas;
    private boolean mTomo_Foto = false;
    //Location
    private LocationManager mLocationManager;
    private double mLongitudeBest;
    private double mLatitudeBest;
    private static final String TAG = "ContenidoFragment";
    //Camara
    private HiddenCameraFragment mHiddenCameraFragment;
    private CameraPreview mCameraPreview;
    private CameraConfig mCachedCameraConfig;
    /*public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK)
            Toast.makeText(getApplicationContext(), "back press",
                    Toast.LENGTH_LONG).show();

        return false;
        // Disable back button..............
    }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        setContentView(R.layout.activity_questionario);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mHomeFormView = findViewById(R.id.quiz_form);
        mProgressView = findViewById(R.id.progressbar_quiz);
        mBtn_Enviar = findViewById(R.id.btn_enviar_preguntas);
        mPregunta = findViewById(R.id.txt_pregunta);
        mRespuesta_1 = findViewById(R.id.rbt_respuesta_1);
        mRespuesta_2 = findViewById(R.id.rbt_respuesta_2);
        mRespuesta_3 = findViewById(R.id.rbt_respuesta_3);
        mRespuesta_4 = findViewById(R.id.rbt_respuesta_4);
        mRespuesta_5 = findViewById(R.id.rbt_respuesta_5);
        mRadioGroup = findViewById(R.id.rdg_respuestas);
        mLbl_total_preguntas = findViewById(R.id.lbl_total_preguntas);
        mPregunta.setText(R.string.lorem_ipsum);
        mRespuesta_1.setText("Respuesta 1");
        mRespuesta_2.setText("Respuesta 2");
        mRespuesta_3.setText("Respuesta 3");
        mRespuesta_4.setText("Respuesta 4");
        mRespuesta_5.setText("Respuesta 5");
        showProgress(true);
        receiveData();
        toggleBestUpdates();
        mPreguntasTask = new ObtenerCuestionarioTask(mContenidoId,mMinutoPausa);
        mPreguntasTask.execute((Void) null);
        //Captura_Bio_Geo_Referencia();
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            View radioButton = mRadioGroup.findViewById(checkedId);
            int index = mRadioGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton)  mRadioGroup.getChildAt(index);
                Actualizar_Respuestas(index);
        }
        });
        /*findViewById(R.id.btn_captura_camara).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Captura_Bio_Geo_Referencia();
            }
        });*/
    }

    private void Captura_Bio_Geo_Referencia() {
        if (mHiddenCameraFragment != null) {    //Remove fragment from container if present
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(mHiddenCameraFragment)
                    .commit();
            mHiddenCameraFragment = null;
        }

        //startService(new Intent(QuestionarioActivity.this, GeoBioService.class));

        Intent serviceIntent = new Intent(QuestionarioActivity.this,GeoBioService.class);
        serviceIntent.putExtra("Curso_Usuario_Id", String.valueOf(mCursoUsuarioId));
        serviceIntent.putExtra("GSPLatitud", String.valueOf(mLatitudeBest));
        serviceIntent.putExtra("GPSLongitud", String.valueOf(mLongitudeBest));
        Log.i(TAG,"varaibles para envio: id=" + mCursoUsuarioId + " latitud=" + mLatitudeBest + " longitud=" + mLongitudeBest);
        startService(serviceIntent);
        mLocationManager.removeUpdates(locationListenerBest);
    }
    private void Actualizar_Respuestas(int indice) {
        String idPregunta;
        String idRespuesta;
        if (mRespuestasPreguntaCurso.size() > 0) {
            idPregunta = mRespuestasPreguntaCurso.get(0).getPregunta_id();
            idRespuesta = mRespuestasPreguntaCurso.get(indice).getRespuesta_id();
            for (Cuestionario c : mCuestionario) {
                if (c.getPregunta_id().equals(idPregunta)) {
                    c.setRespuesta_id(idRespuesta);
                    //mCuestionario.remove(c);
                    //mCuestionario.add(c);
                }
            }
        }
    }
    public void takePicture() {
        if (mCameraPreview != null) {
            if (mCameraPreview.isSafeToTakePictureInternal()) {
                mCameraPreview.takePictureInternal();
            }
        } else {
            throw new RuntimeException("Background camera not initialized. Call startCamera() to initialize the camera.");
        }
    }
    private void receiveData() {
        //RECEIVE DATA VIA INTENT
        Intent i = getIntent();
        mContenidoId = i.getStringExtra("OTRO_CONTENIDO_ID");
        mCursoUsuarioId = i.getStringExtra("CURSO_USUARIO_ID");
        mMinutoPausa = i.getStringExtra("MINUTO_PAUSA");
        //mContadorPausasIntent = i.getStringExtra("CONTADOR_PAUSAS");
    }
    public void Enviar_Preguntas(View view){
        Button botonActual = (Button) view;
        Intent i = new Intent(QuestionarioActivity.this,
                myVideoActivity.class);
        i.putExtra("CONTENIDO_ID", "1");
        i.putExtra("MINUTO_VIDEO", 6001);
        i.putExtra("CONTADOR_PAUSAS", 2);
        //START ACTIVITY
        startActivity(i);
    }
    View.OnClickListener first_radio_listener = new View.OnClickListener(){
        public void onClick(View v) {
            //Your Implementaions...
        }
    };
    public void onRadioButtonClicked(View view) {

    }
    public class ObtenerCuestionarioTask extends AsyncTask<Void,Void,Boolean> {
        private final String mContenidoDetalleId;
        private final String mMilisegundo;
        ObtenerCuestionarioTask(String contenido_detalle_id,String milisegundo) {
            mContenidoDetalleId = contenido_detalle_id;
            mMilisegundo = milisegundo;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                //if(!Conexion.isNetworkAvailableAndConnected(QuestionarioActivity.this) || !Conexion.isOnlineNet() )
                //{

                    //mCuestionario = new HttpContenidos().ObtenerCuestionarioPeticion(mContenidoDetalleId,mMilisegundo,mTipo);
                    //mRespuestas = new HttpContenidos().ObtenerRespuestasPeticion(mContenidoDetalleId,mMilisegundo);
                //}
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mPreguntasTask = null;
            showProgress(false);
            if (success) {
                updateQuestion();
                /*if(ObjResumen!=null){
                    mLblNombre.setText(ObjResumen.getmNombre());
                    mLblNss.setText(ObjResumen.getmNss());
                    mLblCurso.setText(ObjResumen.getmCurso());
                    mLblMotivo.setText(ObjResumen.getmMotivo());
                    mLblEstatus.setText(ObjResumen.getmEstatusCurso());
                    CursoUsuarioId = ObjResumen.getmCursoUsuarioId();
                }*/
            }
            else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        protected void onCancelled() {
            mPreguntasTask = null;
            showProgress(false);
        }
    }
    public class EnviaGPSTask extends AsyncTask<Void,Void,Boolean> {
        private final String mId;
        private final String mLat;
        private final String mLong;
        private String IdRespuesta;
        EnviaGPSTask(String Id, String Lat, String Long) {
            mId = Id;
            mLat = Lat;
            mLong = Long;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                new HttpContenidos().EnviarGeoLocalizacion(mId,mLat,mLong);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mPreguntasTask = null;
            showProgress(false);
            if (success) {
                Log.i(TAG,"Se envio a la base de datos la georeferencia y se procede a tomar foto");
                Captura_Bio_Geo_Referencia();
            }
            else {

            }
        }
        protected void onCancelled() {
            mPreguntasTask = null;
            showProgress(false);
        }
    }
    public class EnviarCuestionarioTask extends AsyncTask<Void,Void,Boolean> {
        private final String mRespuestas;
        private String IdRespuesta;
        EnviarCuestionarioTask(String Cadenajson_Respuestas) {
            mRespuestas = Cadenajson_Respuestas;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                IdRespuesta = new HttpContenidos().EnviarCuestionarioPeticion("","","",mRespuestas);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mPreguntasTask = null;
            showProgress(false);
            if (success) {
                ID_Respuestas = IdRespuesta;

                Intent i = new Intent(getBaseContext(),myVideoActivity.class);
                i.putExtra("CONTENIDO_ID",mContenidoId);
                i.putExtra("CURSO_USUARIO_ID",mCursoUsuarioId);
                i.putExtra("MINUTO_VIDEO", mMinutoPausa);
                //i.putExtra("CONTADOR_PAUSAS", mContadorPausasIntent);
                startActivity(i);
                 finish();

            }
            else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        protected void onCancelled() {
            mPreguntasTask = null;
            showProgress(false);
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mHomeFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mBtn_Enviar.setVisibility(show ? View.GONE : View.VISIBLE);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mBtn_Enviar.setVisibility(show ? View.GONE : View.GONE);
        }
    }
    public void Siguiente_Pregunta(View view){
        //Captura_Bio_Geo_Referencia();
        mCurrentIndex ++;
        if(mCurrentIndex>=mCuestionario.size() )
            mCurrentIndex --;
        else
        updateQuestion();
    }
    public void Anterior_Pregunta(View view){
        //Captura_Bio_Geo_Referencia();
        mCurrentIndex --;
        if(mCurrentIndex<0 )
            mCurrentIndex ++;
        else
        updateQuestion();
    }
    private void updateQuestion(){
        String question = "";// = mCuestionario.get(mCurrentIndex).getNombre_pregunta();
        Cuestionario pregunta;
        String Respuesta_1 = "";
        String Respuesta_2 = "";
        String Respuesta_3 = "";
        String Respuesta_4 = "";
        String Respuesta_5 = "";
        String Respuesta_Seleccionada_ID = "";
        mRespuesta_3.setVisibility(View.VISIBLE);
        mRespuesta_4.setVisibility(View.VISIBLE);
        mRespuesta_5.setVisibility(View.VISIBLE);
        mRespuesta_2.setVisibility(View.VISIBLE);
        mRespuesta_1.setVisibility(View.VISIBLE);
        pregunta = mCuestionario.get(mCurrentIndex);
        question = pregunta.getNombre_pregunta();
        //mRespuestasPreguntaCurso = mRespuestas.stream().filter( respuestas -> respuestas.getPregunta_id().equals(pregunta.getPregunta_id()) ).collect(Collectors.toList());
        mLbl_total_preguntas.setText(mCurrentIndex + 1 + " de " + mCuestionario.size());
        mPregunta.setText(question);
        mRespuestasPreguntaCurso = new ArrayList<>();
        mRadioGroup.clearCheck();
        for(Respuestas r : mRespuestas){
            if(r.getPregunta_id().equals(pregunta.getPregunta_id()))
            mRespuestasPreguntaCurso.add(r);
        }
        switch( mRespuestasPreguntaCurso.size() ) {

            case 2:
                mRespuesta_3.setVisibility(View.INVISIBLE);
                mRespuesta_4.setVisibility(View.INVISIBLE);
                mRespuesta_5.setVisibility(View.INVISIBLE);
            break;
            case 3:
                mRespuesta_4.setVisibility(View.INVISIBLE);
                mRespuesta_5.setVisibility(View.INVISIBLE);
            break;
            case 4:
                mRespuesta_5.setVisibility(View.INVISIBLE);
            break;

        }
        Respuesta_Seleccionada_ID = pregunta.getRespuesta_id();
        int i = 1;
        for( Respuestas r : mRespuestasPreguntaCurso )
        {
            if(i==1)
            {
                mRespuesta_1.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                        if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                            mRespuesta_1.setChecked(true);
                        else
                            mRespuesta_1.setChecked(false);
                }
            }
            if(i==2){
                mRespuesta_2.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_2.setChecked(true);
                    else
                        mRespuesta_2.setChecked(false);
                }
            }
            if(i==3){
                mRespuesta_3.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_3.setChecked(true);
                    else
                        mRespuesta_3.setChecked(false);
                }
            }
            if(i==4){
                mRespuesta_4.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_4.setChecked(true);
                    else
                        mRespuesta_4.setChecked(false);
                }
            }
            if(i==5){
                mRespuesta_5.setText(r.getNombre_respuesta());
                if(!isNullOrBlank(Respuesta_Seleccionada_ID)){
                    if(r.getRespuesta_id().equals(Respuesta_Seleccionada_ID))
                        mRespuesta_5.setChecked(true);
                    else
                        mRespuesta_5.setChecked(false);
                }
            }
            i++;
        }
    }
    public static boolean isNullOrBlank(String param) {
        return param == null || param.trim().length() == 0;
    }
    public  void Btn_Enviar_Click(View view){
        List<String> foo = new ArrayList<String>();
        for(Cuestionario c : mCuestionario){
            if(isNullOrBlank(c.getRespuesta_id())){
                showAlert(getResources().getString(R.string.lbl_send_quiz),getResources().getString(R.string.lbl_send_validation ));
                return;
            }
        }

        String json = new Gson().toJson(mCuestionario);
        showProgress(true);
        mEnviarRespuestasTask = new EnviarCuestionarioTask(json);
        mEnviarRespuestasTask.execute((Void) null);
    }
    private boolean isLocationEnabled(){
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    private void showAlert(String Title, String Mensaje){
        final AlertDialog.Builder mensajeAlerta = new AlertDialog.Builder(this);
        mensajeAlerta.setTitle(Title).setMessage(Mensaje).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Intent myIntent= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //startActivity(myIntent);
            }
        });
        mensajeAlerta.show();
    }
    private void showAlert(){
        final AlertDialog.Builder mensajeAlerta = new AlertDialog.Builder(this);
        mensajeAlerta.setTitle("Habilitar ubicación").setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                "para usar esta app").setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent myIntent= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        mensajeAlerta.show();
    }
    private boolean checkLocation(){
        if(!isLocationEnabled())
            showAlert();
        return  isLocationEnabled();
    }
    public void toggleBestUpdates(){
        if(!checkLocation())
            return;
            mLocationManager.removeUpdates(locationListenerBest);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setCostAllowed(true);
                criteria.setPowerRequirement(Criteria.POWER_LOW);
                String provider = mLocationManager.getBestProvider(criteria,true);
                if(provider!=null){
                    mLocationManager.requestLocationUpdates(provider,2*20*1000,10,locationListenerBest);
                }
            }
    }
    private final LocationListener locationListenerBest = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLongitudeBest = location.getLongitude();
            mLatitudeBest = location.getLatitude();
            Captura_Bio_Geo_Referencia();
            mGpsTask = new EnviaGPSTask("2",String.valueOf(mLatitudeBest),String.valueOf(mLongitudeBest));
            mGpsTask.execute((Void) null);

        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

}
