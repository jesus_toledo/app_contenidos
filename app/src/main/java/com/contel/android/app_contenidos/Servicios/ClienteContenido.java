package com.contel.android.app_contenidos.Servicios;

import android.content.Intent;
import android.util.Log;

import com.contel.android.app_contenidos.Camara.GeoBioService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ClienteContenido {

    private static  String BASE_URL = "http://200.33.34.9/Ws_Contenidos_Pruebas/Ws_Contenidos.asmx/";
    private  static ClienteContenido sClienteContenido;
    private ServiciosContenido mServiciosContenido;
    private SoapApi mSoapApi;

    public ClienteContenido(){

        Strategy strategy = new AnnotationStrategy();
        Serializer serializer = new Persister(strategy);
        OkHttpClient okHttpClient = new OkHttpClient();


        final Gson gson =
                new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        final    Gson gson1 = new  GsonBuilder().disableInnerClassSerialization().create();

        final Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();



        mServiciosContenido = retrofit.create(ServiciosContenido.class);
        mSoapApi = retrofit.create(SoapApi.class);
    }

    public  static ClienteContenido getClienteContenido(){
        if (sClienteContenido == null){
            sClienteContenido = new ClienteContenido();
        }

        return  sClienteContenido;
    }


    public Observable<String> EnviarGeoLocalizacion(String id, String latitud, String longitud){
        return  mServiciosContenido.EnviarGeoLocalizacion(id,latitud,longitud);
    }

    public Observable<String> EnviarReproduccion(String Contenido_ID, String Curso_ID, String Latitud, String Longitud, String Cadena_Foto){
        return  mServiciosContenido.EnviarReproduccion(Contenido_ID,Curso_ID,Latitud,Longitud,Cadena_Foto );
    }

    public  Observable<String> ObtenerPuntosPausas(String contenidosID){
        return  mServiciosContenido.ObtenerPuntosPausas(contenidosID);
    }

    public Call<String> ObtenerPuntosPausas1 (String contenidosID){
        return  mServiciosContenido.ObtenerPuntosPausas1(contenidosID);
    }

    public  Call<String> ObtenerPuntosPausasPdf(String contenidosID){
        return  mServiciosContenido.ObtenerPuntosPausasPdf(contenidosID);
    }


}
