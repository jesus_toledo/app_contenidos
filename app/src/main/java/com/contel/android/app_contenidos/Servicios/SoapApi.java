package com.contel.android.app_contenidos.Servicios;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface   SoapApi {

    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("Obtener_Puntos_Pausa_Preguntas")
    public Response uploadRequest(@Body RequestEnvelope body);
}
