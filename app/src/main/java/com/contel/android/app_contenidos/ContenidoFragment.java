package com.contel.android.app_contenidos;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.contel.android.app_contenidos.Entidades.Contenido;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;
import com.contel.android.app_contenidos.Validaciones.Conexion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jtoledo on 18/04/2018.
 */

public class ContenidoFragment extends Fragment {
    private static final String TAG = "ContenidoFragment";
    private static final String KEY_INDEX = "index";
    private static final String KEY_BREAK_PONT_QUIZ_INDEX = "break_point_index";
    private static final String ARRAY_KEYS_PAUSE = "array_puntos_pausa";
    private static final String KEY_VIDEO_PROGRESS = "video_progress";
    private static final String KEY_PAUSAS_DELAY = "contador_pausas_delay";
    private int mActivityOrientation;
    private static int duration = 0;
    private Contenido mContenido;
    private TextView mTitleField;
    private TextView mDateButton;
    private Button mBtn_Back;
    private VideoView mVideoView;
    private ProgressBar mProgressBar;
    private int mVideoPosition = 0;
    ProgressDialog progDailog;
    private String mVideoPausaPosition = "0";
    private View mProgressView;
    private TextView mTxtVideoCurrentPos;
    private String mContenidoId;
    private String mCursoUsuarioId;
    private String uriPath;
    int mContadorPausasQuestionario = 0;
    int mContadorPausasQuestionario_delay = 0;
    private boolean mEntroCreate = false;
    private ConsultarBreakPointsTask mTaskPeticionQuiz = null;
    private String[] mArrayPuntosPausaPreguntas;
    private MediaController mediaController;
    private View mView;
    private Timer mTimer;

    /*public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU)
            Toast.makeText(getActivity(), "VIDEO EN CURSO",
                    Toast.LENGTH_LONG).show();
        return false;
        // Disable back button..............
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        Log.d(TAG, this + ":Fragment onCreated()");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, this + ": onViewCreated()");

        if (savedInstanceState != null) {
            //mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mVideoPosition = savedInstanceState.getInt(KEY_VIDEO_PROGRESS);
            mContadorPausasQuestionario_delay = savedInstanceState.getInt(KEY_PAUSAS_DELAY);
            Log.i(TAG, "si hay instancia guardada " + mVideoPosition);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_contenido, container, false);
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                boolean regreso = true;
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                            Toast.makeText(getActivity(), "VIDEO EN CURSO",
                                    Toast.LENGTH_SHORT).show();
                        regreso = false;
                    }
                    return regreso;
            }
        });
        Activity a = getActivity();
        if(a != null)
            a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        mVideoView = mView.findViewById(R.id.myVideo);
        mProgressView = mView.findViewById(R.id.video_download_progress);
        mTitleField = mView.findViewById(R.id.crime_title);
        mTxtVideoCurrentPos = mView.findViewById(R.id.videoCurrentPos);
        mProgressBar = mView.findViewById(R.id.Progressbar);
        mDateButton = mView.findViewById(R.id.crime_date);
        mBtn_Back = mView.findViewById(R.id.btn_fr_video_back);
        mActivityOrientation = getResources().getConfiguration().orientation;
        if (mActivityOrientation == Configuration.ORIENTATION_LANDSCAPE)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        else
            mTxtVideoCurrentPos.setVisibility(View.VISIBLE);
        mBtn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContenidoListFragment fr=new ContenidoListFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,fr)
                        .addToBackStack(null)
                        .commit();
            }
        });
        receiveData();
        Cargar_Datos();
        return mView;
    }

    private void Cargar_Datos() {
        mContenido = ContenidoLab.get(getActivity(), "0").getContenido(mContenidoId);
        mTitleField.setText(mContenido.getNombre_contenido());
        mDateButton.setText(mContenido.getEstatus());
        mTaskPeticionQuiz = new ConsultarBreakPointsTask(mContenidoId);
        mTaskPeticionQuiz.execute((Void) null);
        progDailog = ProgressDialog.show(getActivity(), "Espere ...", "Cargando video ...", true);
        if(!Conexion.isNetworkAvailableAndConnected(getActivity()) || !Conexion.isOnlineNet() ){
            progDailog.dismiss();
            showAlert("Contenidos",getResources().getString(R.string.connection_error));
        }
        else {
            if (mContenido.getStreaming().equals("NO"))
                Descargar_Contenido();
            else
                setUpVideoView();    }

    }
    private void setUpVideoView() {
        if (mContenido.getStreaming().equals("SI"))
            uriPath = mContenido.getUrl_acceso();
        mediaController = new MediaController(getActivity());
        // Asigna los controles multimedia a la VideoView.
        mVideoView.setMediaController(mediaController);
        try {
            // Asigna la URI del vídeo que será reproducido a la vista.
            mVideoView.setVideoPath(uriPath);
            // Se asigna el foco a la VideoView.
            mVideoView.requestFocus();
        }  catch (Exception e) {
            progDailog.dismiss();
            showAlert("Contenidos", e.getMessage());
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        /*
         * Se asigna un listener que nos informa cuando el vídeo
         * está listo para ser reproducido.
         */
        mVideoView.setOnPreparedListener(videoViewListener);
    }
    private void Descargar_Contenido() {
        if (getActivity() == null)
            return;
        String[] Partes_Url;
        String url = mContenido.getUrl_acceso();
        String externalStroageString = String.valueOf(getActivity().getFilesDir());//+"/Pdf/";
        Partes_Url = url.split("/");
        String fileName = "/" + Partes_Url[Partes_Url.length - 1];
        //File file = new File(externalStroageString);
        File file = new File(Environment.getExternalStoragePublicDirectory(String.valueOf(getActivity().getFilesDir() + "/")), fileName);
        uriPath = file.getPath();

        File dir = new File(externalStroageString);
        if (dir.exists() == false) {
            dir.mkdirs();
        }
        if (file.isFile() == false) {
            //showProgress(true);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            request.setDescription(fileName);
            request.setTitle("Contenidos App");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(externalStroageString, fileName); //Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        } else {
            try {
                setUpVideoView();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Toast.makeText(getActivity(), "Se descargó Video", Toast.LENGTH_SHORT).show();
            try {
                showProgress(false);
                setUpVideoView();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
    private MediaPlayer.OnPreparedListener videoViewListener = new MediaPlayer.OnPreparedListener() {
        int posicion_video = 0;
        int minuto_pausa = 0;
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            progDailog.dismiss();
            duration = mVideoView.getDuration();
            mVideoView.requestFocus();

            Log.i(TAG,"el contador va en: " + mContadorPausasQuestionario);
            Log.i(TAG,"el delay va en: " + mContadorPausasQuestionario_delay);
            if( mContadorPausasQuestionario_delay == mContadorPausasQuestionario)
                mVideoView.seekTo(mVideoPosition);
            else{
                minuto_pausa = Integer.parseInt(mArrayPuntosPausaPreguntas[mContadorPausasQuestionario_delay]);
                if(mVideoView.getCurrentPosition()<= minuto_pausa)
                    mVideoView.seekTo(minuto_pausa);
                else
                    mVideoView.seekTo(mVideoPosition);
            }
            mVideoView.start();
            timerCounter();
        }
    };
    private void timerCounter(){
        mTimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if(getActivity() == null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                        Abrir_Cuestionario();
                    }
                });
            }
        };
        mTimer.schedule(task, 0, 100);
    }

    private void updateUI(){
        int current = mVideoView.getCurrentPosition();
        int progress = current * 100 / duration;
        //Log.i(TAG,"El video va en "+progress + "%");
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.setProgress(progress);
        //Log.i(TAG,"Valor de la barra: "+mProgressBar.getProgress() + "%");
        if (mProgressBar.getProgress() >= 99) {
            Log.i(TAG,"Barra de progreso: " + mProgressBar.getProgress());
            mTimer.cancel();
            ContenidoListFragment fr=new ContenidoListFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container,fr)
                    .addToBackStack(null)
                    .commit();
        }
    }
    private void Abrir_Cuestionario() {
        int minuto_pausa = 0;
        if(mArrayPuntosPausaPreguntas!=null) {
            if (mContadorPausasQuestionario < mArrayPuntosPausaPreguntas.length) {
                minuto_pausa = Integer.parseInt(mArrayPuntosPausaPreguntas[mContadorPausasQuestionario]);
                if(mVideoView.getCurrentPosition()>=minuto_pausa){
                    QuestionaryFragment fr=new QuestionaryFragment();
                    Bundle bn = new Bundle();
                    bn.putString("CONTENIDO_ID", mContenido.getContenido_id());
                    bn.putString("CURSO_USUARIO_ID", mCursoUsuarioId);
                    bn.putInt("CONTADOR_PAUSAS", mContadorPausasQuestionario);
                    bn.putInt("CONTADOR_PAUSAS_DELAY", mContadorPausasQuestionario_delay);
                    bn.putString("MINUTO_PAUSA", String.valueOf(minuto_pausa));
                    bn.putString("TIPO_CONTENIDO", "video");
                    fr.setArguments(bn);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,fr)
                            .addToBackStack(null)
                            .commit();
                }
                else
                    mContadorPausasQuestionario_delay = mContadorPausasQuestionario;
            }
            else
                mContadorPausasQuestionario_delay = mContadorPausasQuestionario;
        }

    }
    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        Log.d(TAG, this + ": onDestroyView()");
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        Log.d(TAG, this + ": onDetach()");
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Log.d(TAG, this + ": onStart()");
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d(TAG, this + ": onResume()");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        Log.d(TAG, this + ": onPause()");
    }

    @Override
    public void onStop()
    {
        super.onStop();
        Log.d(TAG, this + ": onStop()");
    }

    @Override
    public void onDestroy()
    {
        mTimer.cancel();
        super.onDestroy();
        Log.d(TAG, this + ": onDestroy()");
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(KEY_VIDEO_PROGRESS, mVideoView.getCurrentPosition());
        savedInstanceState.putInt(KEY_PAUSAS_DELAY, mContadorPausasQuestionario_delay);
    }

    private void receiveData() {
        Bundle bundle=getArguments();
        mContenidoId = bundle.getString("CONTENIDO_ID");
        mCursoUsuarioId = bundle.getString("CURSO_USUARIO_ID");
        mContadorPausasQuestionario = bundle.getInt("CONTADOR_PAUSAS");
        mContadorPausasQuestionario_delay = bundle.getInt("CONTADOR_PAUSAS_DELAY");
    }
    public class ConsultarBreakPointsTask extends AsyncTask<Void, Void, Boolean> {
        Boolean blnAcceso = false;
        private final String mContid;
        //private final String mPassword;

        ConsultarBreakPointsTask(String contenidoId) {
            mContid = contenidoId;
            //mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(1);
                mArrayPuntosPausaPreguntas = new HttpContenidos().PasuaPreguntasPeticion(mContid);//
            } catch (InterruptedException e) {
                return false;
            }
            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mTaskPeticionQuiz = null;
            if (success) {
            }
        }

        @Override
        protected void onCancelled() {
            mTaskPeticionQuiz = null;

        }
    }
    private void showAlert(String Title, String Mensaje){
        final AlertDialog.Builder mensajeAlerta = new AlertDialog.Builder(getActivity());
        mensajeAlerta.setTitle(Title).setMessage(Mensaje).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Intent myIntent= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //startActivity(myIntent);
            }
        });
        mensajeAlerta.show();
    }
}
