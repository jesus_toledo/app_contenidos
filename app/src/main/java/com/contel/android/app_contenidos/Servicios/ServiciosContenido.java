package com.contel.android.app_contenidos.Servicios;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ServiciosContenido {


    @GET("Registrar_Geolocalizacion")
    Observable<String> EnviarGeoLocalizacion(@Query("ID") String ID, @Query("Latitud") String Latitud, @Query("Longitud") String Longitud);

    @GET("Registrar_Reproduccion")
    Observable<String> EnviarReproduccion(@Query("ContenidoId") String Contenido_ID, @Query("CursoId") String Curso_ID, @Query("Latitud") String Latitud , @Query("Longitud") String Longitud , @Query("base64String") String Cadena_Foto);

    @GET("Obtener_Puntos_Pausa_Preguntas")
    Observable<String> ObtenerPuntosPausas(@Query("contenido_id") String contenidosID);


    @Headers({"Content-Type: text/plain",
            "Accept-Charset: utf-8"
    })
    @GET("Obtener_Puntos_Pausa_Preguntas")
    Call<String> ObtenerPuntosPausas1(@Query("contenido_id") String contenidosID);

    @Headers({"Content-Type: text/plain",
            "Accept-Charset: utf-8"
    })
    @GET("Obtener_Puntos_Pausa_Preguntas_Pdf")
    Call<String> ObtenerPuntosPausasPdf(@Query("contenido_id") String contenidosID);


}
