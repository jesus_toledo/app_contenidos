package com.contel.android.app_contenidos;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.VideoView;

public class ContenidosActivity extends SingleFragmentActivity {
    public static final String EXTRA_CONTENIDO_ID = "com.contel.android.contenidos.contenido_id";
    public static final String EXTRA_CURSO_ID = "com.contel.android.contenidos.curso_id";
    public static Intent newIntent(Context packageContext, String contenidoId, String cursoId) {
        Intent intent = new Intent(packageContext, ContenidosActivity.class);
        intent.putExtra(EXTRA_CONTENIDO_ID, contenidoId);
        intent.putExtra(EXTRA_CURSO_ID, cursoId);
        return intent;
    }
    protected Fragment createFragment()
    {
        return new ContenidoFragment();
    }

}
