package com.contel.android.app_contenidos.Camara;

import android.support.annotation.NonNull;

import java.io.File;

/**
 * Created by jtoledo on 10/05/2018.
 */

public interface CameraCallbacks {
    void onImageCapture(@NonNull File imageFile);

    void onCameraError(@CameraError.CameraErrorCodes int errorCode);
}
