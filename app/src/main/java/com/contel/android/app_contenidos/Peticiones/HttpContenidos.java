package com.contel.android.app_contenidos.Peticiones;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.contel.android.app_contenidos.Entidades.Contenido;
import com.contel.android.app_contenidos.Entidades.Cuestionario;
import com.contel.android.app_contenidos.Entidades.Respuestas;
import com.contel.android.app_contenidos.Entidades.Resumen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static java.net.Proxy.Type.HTTP;

/**
 * Created by jtoledo on 16/04/2018.
 */

public class HttpContenidos {
    String function,data;
    private static final String TAG = "ContenidoFragment";
    private static final String API_KEY = "yourApiKeyHere";
    private static  String BASE_URL = "http://200.33.34.9/Ws_Contenidos_Pruebas/Ws_Contenidos.asmx/";

    public String enviar_imagen(String imagenURL, String Id, String urlString){
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        DataInputStream inStream = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024*1024;
        try
        {
            java.net.URL url = new java.net.URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            String post="POST";
            conn.setRequestMethod(post);
            String keep="Keep-Alive";
            String connection="Connection";
            conn.setRequestProperty(connection, keep);
            String multipart="multipart/form-data;boundary="+boundary;
            String imagen = "Content-Disposition: form-data; ID=\""+Id +"\";bas64String=\"" + imagenURL + "\"";
            conn.setRequestProperty("Content-Type", multipart);
            dos = new DataOutputStream( conn.getOutputStream() );
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes(imagen);
            dos.writeBytes(lineEnd);
            int totalRead = 0;
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            dos.flush();
            dos.close();
        }
        catch(SocketException ex)
        {}
        catch (MalformedURLException ex)
        {}
        catch (IOException ioe)
        {}

        return "Ok";
    }

    public byte[] getUrlBytes(String urlSpec, boolean isPost) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        if(isPost)
        connection.setRequestMethod("POST");
        else
        connection.setRequestMethod("GET");

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();

            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }
    public String getUrlString(String urlSpec, boolean isPost) throws IOException {
        return new String(getUrlBytes(urlSpec,isPost));
    }
    public boolean loginPeticion(String ssn) {
        boolean respuesta = false;
        String strRespuesta = "";
        try {
            String url = Uri.parse(BASE_URL + "Acceso")
                    .buildUpon()
                    .appendQueryParameter("Nss", ssn)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            //Log.i(TAG,jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            strRespuesta = jsonBody.getString("result");
            if(strRespuesta.equals("True"))
                respuesta = true;

        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (JSONException ioe) {
            Log.e(TAG, "Failed to parse items", ioe);
        }
        return respuesta;
    }
    public Resumen ResumenPeticion(String ssn) {
        Resumen respuesta = null;
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Datos_Usuario")
                    .buildUpon()
                    .appendQueryParameter("Nss", ssn)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            //Log.i(TAG,jsonString);
            Gson gson = new GsonBuilder().create();
            respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }
    public List<Contenido> ObtenerContenidos(String CursoUsuarioId) {
        List<Contenido> respuesta = null;
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Contenidos")
                    .buildUpon()
                    .appendQueryParameter("CursoUsuarioId", CursoUsuarioId)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            //Log.i(TAG,jsonString);
            Gson gson = new GsonBuilder().create();
            Type type = new TypeToken<List<Contenido>>(){}.getType();
            respuesta = gson.fromJson(jsonString, type);

            //respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }

    public String[] PasuaPreguntasPeticion(String contenido_id) {
        String[] respuesta = {};
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Puntos_Pausa_Preguntas")
                    .buildUpon()
                    .appendQueryParameter("contenido_id", contenido_id)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            respuesta = jsonString.split(",");
            /*Gson gson = new GsonBuilder().create();
            Type type = new TypeToken<List<Contenido>>(){}.getType();
            respuesta = gson.fromJson(jsonString, type);*/

            //respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }
    public String[] PasuaPreguntasPdfPeticion(String contenido_id) {
        String[] respuesta = {};
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Puntos_Pausa_Preguntas_Pdf")
                    .buildUpon()
                    .appendQueryParameter("contenido_id", contenido_id)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            respuesta = jsonString.split(",");
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }

    public List<Cuestionario> ObtenerCuestionarioPeticion(String mContenidoDetalleId, String mMilisegundo, String Tipo) {
        List<Cuestionario> respuesta = null;
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Questionario")
                    .buildUpon()
                    .appendQueryParameter("Contenido_Id", mContenidoDetalleId)
                    .appendQueryParameter("Milisegundo", mMilisegundo)
                    .appendQueryParameter("Tipo", Tipo)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            //Log.i(TAG,jsonString);
            Gson gson = new GsonBuilder().create();
            Type type = new TypeToken<List<Cuestionario>>(){}.getType();
            respuesta = gson.fromJson(jsonString, type);

            //respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }
    public String ObtenerIdCuestionario(String mContenidoDetalleId, String mMilisegundo, String Tipo) {
        String respuesta = "0";
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Questionario_ID")
                    .buildUpon()
                    .appendQueryParameter("Contenido_Id", mContenidoDetalleId)
                    .appendQueryParameter("Milisegundo", mMilisegundo)
                    .appendQueryParameter("Tipo", Tipo)
                    .build().toString();
            String jsonString = getUrlString(url,false);
            //Log.i(TAG,jsonString);
            respuesta = jsonString;

            //respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }
    public List<Respuestas> ObtenerRespuestasPeticion(String mContenidoDetalleId, String mMilisegundo, String Tipo) {
        List<Respuestas> respuesta = null;
        try {
            String url = Uri.parse(BASE_URL + "Obtener_Respuestas")
                    .buildUpon()
                    .appendQueryParameter("Contenido_Id", mContenidoDetalleId)
                    .appendQueryParameter("Milisegundo", mMilisegundo)
                    .appendQueryParameter("Tipo", Tipo)
                    .build().toString();
            String jsonString = getUrlString(url, false);
            //Log.i(TAG,jsonString);
            Gson gson = new GsonBuilder().create();
            Type type = new TypeToken<List<Respuestas>>(){}.getType();
            respuesta = gson.fromJson(jsonString, type);

            //respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return respuesta;
    }
    public String EnviarCuestionarioPeticion(String Curso_Usuario_ID,String Contenido_ID,String Cuestionario_ID, String mContenidoDetalleId) {
        List<Cuestionario> respuesta = null;
        String jsonString = "";
        try {
            String url = Uri.parse(BASE_URL + "Recibir_Respuestas")
                    .buildUpon()
                    .appendQueryParameter("Curso_Usuario_ID", Curso_Usuario_ID)
                    .appendQueryParameter("Contenido_ID", Contenido_ID)
                    .appendQueryParameter("Cuestionario_ID", Cuestionario_ID)
                    .appendQueryParameter("Obj_Respuestas", mContenidoDetalleId)
                    .build().toString();
                    jsonString = getUrlString(url, false);
//            Gson gson = new GsonBuilder().create();
//            Type type = new TypeToken<List<Cuestionario>>(){}.getType();
//            respuesta = gson.fromJson(jsonString, type);

            //respuesta = gson.fromJson(jsonString, Resumen.class);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return jsonString;
    }
    public int EnviarGeoLocalizacion(String Id, String mLatitud, String mLongitud) {
        try {
            String url = Uri.parse(BASE_URL + "Registrar_Geolocalizacion")
                    .buildUpon()
                    .appendQueryParameter("ID", Id)
                    .appendQueryParameter("Latitud", mLatitud)
                    .appendQueryParameter("Longitud", mLongitud)
                    .build().toString();
            String jsonString = getUrlString(url, false);
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to fetch items", ioe);
        } catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return 1;
    }

    public int EnviarFoto(String ContenidoId, String CursoId,String Latitud,String Longitud, String CadenaFoto) {
        StringBuffer response = null;
        try {
            URL obj = new URL(BASE_URL + "Registrar_Reproduccion");
            data = "ContenidoId=" + ContenidoId + "&CursoId=" + CursoId + "&Latitud=" + Latitud + "&Longitud=" + Longitud + "&base64String=" + URLEncoder.encode(CadenaFoto,"UTF-8");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept-Language","en-US,en;q=0.5");
            con.setDoOutput(true);
            con.setFixedLengthStreamingMode(data.getBytes().length);
            con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            OutputStream out = new BufferedOutputStream(con.getOutputStream());
            out.write(data.getBytes());
            out.flush();
            out.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while((inputLine = in.readLine()) != null)
            {
                response.append(inputLine);
            }
            in.close();
            //enviar_imagen(CadenaFoto,Id, "http://200.33.34.9/Ws_Contenidos/Ws_Contenidos.asmx/Subir_Foto");

            /*String url = Uri.parse("http://200.33.34.9/Ws_Contenidos/Ws_Contenidos.asmx/Subir_Foto")
                    .buildUpon()
                    .appendQueryParameter("ID", Id)
                    .appendQueryParameter("base64String", CadenaFoto)
                    .build().toString();
            String jsonString = getUrlString(url, true);*/
        }
        catch(MalformedURLException e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        catch (ProtocolException e){
            Log.e(TAG, "Failed to parse items", e);
        }
        catch (UnsupportedEncodingException e){
            Log.e(TAG, "Failed to parse items", e);
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to parse items", e);
        }
        return 1;
    }
}