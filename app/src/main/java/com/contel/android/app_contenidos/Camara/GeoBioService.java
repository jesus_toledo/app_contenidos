package com.contel.android.app_contenidos.Camara;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.contel.android.app_contenidos.Camara.CameraConfig;
import com.contel.android.app_contenidos.Camara.CameraError;
import com.contel.android.app_contenidos.Camara.HiddenCameraService;
import com.contel.android.app_contenidos.Camara.HiddenCameraUtils;
import com.contel.android.app_contenidos.Camara.config.CameraFacing;
import com.contel.android.app_contenidos.Camara.config.CameraImageFormat;
import com.contel.android.app_contenidos.Camara.config.CameraResolution;
import com.contel.android.app_contenidos.Camara.config.Foto_Util;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.function.LongToIntFunction;

/**
 * Created by jtoledo on 10/05/2018.
 */

public class GeoBioService extends HiddenCameraService {
    private static final String TAG = "ContenidoFragment";
    private EnviaFotoTask mEnviarFotoTask = null;
    private String mBestLongitud;
    private String mBestLatitud;
    private String mCursoUsuarioId;
    private String mContenido_Id;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();

        if(extras == null) {
            Log.d(TAG,"los extras son nulos");
        } else {
            Log.d("Service","not null");
            mBestLatitud = String.valueOf(extras.get("GSPLatitud"));
            mBestLongitud = String.valueOf(extras.get("GPSLongitud"));
            mCursoUsuarioId = String.valueOf(extras.get("Curso_Usuario_Id"));
            mContenido_Id = String.valueOf(extras.get("Contenido_Id"));
            Log.i(TAG,"si llego bien el dato: contenido_id="+mContenido_Id + " curso_usuario=" + mCursoUsuarioId);
            //if(from.equalsIgnoreCase("Main"))
                //StartListenLocation();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            if (HiddenCameraUtils.canOverDrawOtherApps(this)) {
                CameraConfig cameraConfig = new CameraConfig()
                        .getBuilder(this)
                        .setCameraFacing(CameraFacing.FRONT_FACING_CAMERA)
                        .setCameraResolution(CameraResolution.HIGH_RESOLUTION)
                        .setImageFormat(CameraImageFormat.FORMAT_JPEG)
                        .build();

                startCamera(cameraConfig);

                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        takePicture();
                    }
                }, 2000);
            } else {
                Log.i(TAG,"No hay permisos de camara");
                //Open settings to grant permission for "Draw other apps".
                HiddenCameraUtils.openDrawOverPermissionSetting(this);
            }
        } else {
            //TODO Ask your parent activity for providing runtime permission
            Toast.makeText(this, "Camera permission not available", Toast.LENGTH_SHORT).show();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onImageCapture(@NonNull File imageFile) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
        Bitmap bitmap_scalado = Foto_Util.escalarBitmap(imageFile.getAbsolutePath(),4);
        //Do something with the bitmap
        Log.i(TAG,"Ya se tomo la foto");
        mEnviarFotoTask = new EnviaFotoTask(mContenido_Id,mCursoUsuarioId,getStringImagen(bitmap_scalado),mBestLatitud,mBestLongitud);
        mEnviarFotoTask.execute((Void) null);
        stopSelf();
    }

    @Override
    public void onCameraError(@CameraError.CameraErrorCodes int errorCode) {
        switch (errorCode) {
            case CameraError.ERROR_CAMERA_OPEN_FAILED:
                //Camera open failed. Probably because another application
                //is using the camera
                Toast.makeText(this, "Cannot open camera.", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_IMAGE_WRITE_FAILED:
                //Image write failed. Please check if you have provided WRITE_EXTERNAL_STORAGE permission
                Toast.makeText(this, "Cannot write image captured by camera.", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_CAMERA_PERMISSION_NOT_AVAILABLE:
                //camera permission is not available
                //Ask for the camera permission before initializing it.
                Toast.makeText(this, "Camera permission not available.", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_OVERDRAW_PERMISSION:
                //Display information dialog to the user with steps to grant "Draw over other app"
                //permission for the app.
                HiddenCameraUtils.openDrawOverPermissionSetting(this);
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_FRONT_CAMERA:
                Toast.makeText(this, "Your device does not have front camera.", Toast.LENGTH_LONG).show();
                break;
        }

        stopSelf();
    }
    public String getStringImagen(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    public class EnviaFotoTask extends AsyncTask<Void,Void,Boolean> {
        private final String mCursoId;
        private final String mContendioId;
        private final String mCadenaFoto;
        private final String mLatitud;
        private final String mLongitud;
        private String IdRespuesta;
        EnviaFotoTask(String contenido_id, String CursoId, String CadenaFoto, String Latitud, String Longitud) {
            mContendioId = contenido_id;
            mCursoId = CursoId;
            mCadenaFoto = CadenaFoto;
            mLatitud = Latitud;
            mLongitud = Longitud;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                new HttpContenidos().EnviarFoto(mContendioId,mCursoId,mLatitud, mLongitud,mCadenaFoto);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mEnviarFotoTask = null;
            if (success) {
                Log.i(TAG,"Envio de georeferencia exitoso: id=" + mContenido_Id + " latitud=" + mLatitud + " longitud=" + mLongitud + " foto=" + mCadenaFoto);
            }
            else {
                Log.i(TAG,"Envio de georeferencia fallido");
            }
        }
        protected void onCancelled() {
            mEnviarFotoTask = null;
        }
    }
}
