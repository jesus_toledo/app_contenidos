package com.contel.android.app_contenidos;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.contel.android.app_contenidos.Entidades.Contenido;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;
import com.contel.android.app_contenidos.Servicios.ClienteContenido;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;

public class PdfFragment extends Fragment {
    private static final String TAG = "ContenidoFragment";
    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";
    private static final String EXTRA_SOCIAL_SECURITY_N = "com.contel.android.app_contenidos.StrSsn";
    private static final int MIN_SWIPPING_DISTANCE = 50;
    private static final int THRESHOLD_VELOCITY = 50;
    private View mView;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Bitmap mBitmap;
    private ImageView mImageView;
    private Button mButtonNext, mButtonPrevious;
    private View mProgressView;
    private TextView mLblCurrentPage;
    private PdfRenderer.Page mCurrentPage;
    int mContadorPausasQuestionario = 0;
    int mContadorPausasQuestionario_delay = 0;
    private ConsultaRutaPdfTask mConsultarRutaPdf;
    private static String FILENAME = "ejemplo1.pdf";
    private ParcelFileDescriptor mFileDescriptor;
    private PdfRenderer mPdfRenderer;
    private int mPageIndex;
    private String mContenidoId;
    private String mCursoUsuarioId;
    private Contenido mContenido;
    private String[] mArrayPuntosPausaPreguntas;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private String mPaginaPausa;

    public PdfFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PdfFragment newInstance(String param1, String param2) {
        PdfFragment fragment = new PdfFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_pdf, container, false);
        Activity a = getActivity();
        if(a != null)
            a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mImageView = mView.findViewById(R.id.image_pdf_viewer);//(com.contel.android.app_contenidos.Gesture.GestureImageView) findViewById(R.id.image_pdf_viewer);
        mButtonPrevious = mView.findViewById(R.id.previous);
        mButtonNext = mView.findViewById(R.id.next);
        mLblCurrentPage = mView.findViewById(R.id.lbl_current_page);
        mProgressView = mView.findViewById(R.id.pdf_download_progress);
        mPageIndex = 0;
        receiveData();
        obtenerPuntosPausas(mContenidoId);
        if (null != savedInstanceState) {
            mPageIndex = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 0);
        }

        mButtonPrevious.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                //receiveData();
                showPage(mCurrentPage.getIndex() - 1);
            }
        });
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                int next_page = 0;
                int pagina_pausa = 0;
                boolean pasa_hoja = true;

                if(mButtonNext.getText().equals(getResources().getString(R.string.close_pdf))){
                    Salir_Pdf();
                    return;
                }

                if(mArrayPuntosPausaPreguntas!=null) {
                    if (mContadorPausasQuestionario < mArrayPuntosPausaPreguntas.length) {
                        pagina_pausa = Integer.parseInt(mArrayPuntosPausaPreguntas[mContadorPausasQuestionario]);
                        if (mCurrentPage.getIndex() + 1 == pagina_pausa) {
                            pasa_hoja = false;
                        }
                    }
                }
                if(pasa_hoja)
                {
                    showPage(mCurrentPage.getIndex() + 1);
                }
                else
                {
                    QuestionaryFragment fr = new QuestionaryFragment();
                    Bundle bn = new Bundle();
                    bn.putString("CONTENIDO_ID", mContenido.getContenido_id());
                    bn.putString("CURSO_USUARIO_ID", mCursoUsuarioId);
                    bn.putInt("CONTADOR_PAUSAS", mContadorPausasQuestionario);
                    bn.putInt("CONTADOR_PAUSAS_DELAY", mContadorPausasQuestionario_delay);
                    bn.putString("MINUTO_PAUSA", String.valueOf(pagina_pausa));
                    bn.putString("TIPO_CONTENIDO", "pdf");
                    fr.setArguments(bn);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, fr)
                            .addToBackStack(null)
                            .commit();
                }

            }
        });
        return mView;
    }

    public  void  obtenerPuntosPausas(String contenidoId){
        ClienteContenido.getClienteContenido()
                .ObtenerPuntosPausasPdf(contenidoId)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {

                        if(response.isSuccessful()){
                            String datos = response.body();
                            String[] respuesta = {};
                            respuesta = datos.split(",");
                            mArrayPuntosPausaPreguntas = respuesta;
                        }
                        else
                            Log.i(TAG,"No fue exitoso");
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("Error", t.getMessage());
                    }
                });


    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        Log.d(TAG, this + ": onDestroyView()");
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStart()
    {
        super.onStart();
        Descargar_Contenido();
        Log.d(TAG, this + ": onStart()");
    }
    @Override
    public void onPause()
    {
        super.onPause();
        Log.d(TAG, this + ": onPause()");
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStop()
    {
        try {
            closeRenderer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
        Log.d(TAG, this + ": onStop()");
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(TAG, this + ": onDestroy()");
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (null != mCurrentPage) {
            savedInstanceState.putInt(STATE_CURRENT_PAGE_INDEX, mCurrentPage.getIndex());
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void Descargar_Contenido() {
        String[] Partes_Url;
        String url = mContenido.getUrl_acceso();
        int next_page = 0;
        String externalStroageString = String.valueOf( getContext().getFilesDir() );//+"/Pdf/";
        Partes_Url = url.split("/");
        String fileName="/" + Partes_Url[Partes_Url.length-1];
        FILENAME = fileName;
        //File file = new File(externalStroageString);
        File file = new File(Environment.getExternalStoragePublicDirectory(String.valueOf(getContext().getFilesDir()+"/")), fileName);
        File dir = new File (externalStroageString);
        if(dir.exists()==false) {
            dir.mkdirs();
        }
        if (file.isFile()==false) {
            showProgress(true);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            getContext().registerReceiver(onComplete,new IntentFilter( DownloadManager.ACTION_DOWNLOAD_COMPLETE ));
            request.setDescription(FILENAME);
            request.setTitle("Contenidos App");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(externalStroageString , fileName); //Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }
        else{ try {
            openRenderer(getActivity());
            next_page = mPageIndex;
            if(null != mPaginaPausa)
            {
                if(Integer.parseInt(mPaginaPausa)>0)
                    next_page = Integer.parseInt(mPaginaPausa);

            }
            showPage(next_page);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } }
    }
    BroadcastReceiver onComplete=new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void onReceive(Context ctxt, Intent intent) {
            Toast.makeText(getActivity(),"Se descargó PDF", Toast.LENGTH_SHORT).show();
            try {
                showProgress(false);
                openRenderer(getActivity());
                showPage(mPageIndex);

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openRenderer(Context context) throws IOException {

        // In this sample, we read a PDF from the assets directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(String.valueOf(getContext().getFilesDir()+"/")), FILENAME);

        if (!file.exists()) {

            // Since PdfRenderer cannot handle the compressed asset file directly, we copy it into

            // the cache directory.

            InputStream asset = context.getAssets().open(FILENAME);

            FileOutputStream output = new FileOutputStream(file);

            final byte[] buffer = new byte[1024];

            int size;

            while ((size = asset.read(buffer)) != -1) {

                output.write(buffer, 0, size);

            }
            asset.close();
            output.close();
        }

        mFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

        // This is the PdfRenderer we use to render the PDF.

        if (mFileDescriptor != null) {

            mPdfRenderer = new PdfRenderer(mFileDescriptor);

        }

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void closeRenderer() throws IOException {
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        mPdfRenderer.close();
        mFileDescriptor.close();
    }
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mImageView.setVisibility(show ? View.GONE : View.VISIBLE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    mImageView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mImageView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showPage(int index) {
        if(index<0)
            index=0;
        if (mPdfRenderer.getPageCount() < index) {
            return;
        }
        // Make sure to close the current page before opening another one.
        if (null != mCurrentPage) {
            mCurrentPage.close();
        }
        showProgress(true);
        // Use `openPage` to open a specific page in PDF.
        mCurrentPage = mPdfRenderer.openPage(index);
        // Important: the destination bitmap must be ARGB (not RGB).
        mBitmap = Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(),
                Bitmap.Config.ARGB_8888);
        // Here, we render the page onto the Bitmap.
        // To render a portion of the page, use the second and third parameter. Pass nulls to get
        // the default result.
        // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
        mCurrentPage.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        // We are ready to show the Bitmap to user.
        //mImageView = new ImageView(getApplication());//new com.contel.android.app_contenidos.Gesture.GestureImageView(this);
        mImageView.setImageBitmap(mBitmap);
        //mImageView.reset(); //setImageBitmap(mBitmap);
        updateUi();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void Salir_Pdf() {
        ContenidoListFragment fr=new ContenidoListFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,fr)
                .addToBackStack(null)
                .commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void updateUi() {
        String lbl_progress = getResources().getString(R.string.current_pdf_page);
        int index = mCurrentPage.getIndex();
        int pageCount = mPdfRenderer.getPageCount();
        mLblCurrentPage.setVisibility(View.VISIBLE);
        mButtonPrevious.setEnabled(0 != index);
        if(!((index + 1) < pageCount))
            mButtonNext.setText(getResources().getString(R.string.close_pdf));
        else
            mButtonNext.setText(getResources().getString(R.string.next_pdf_page));
        //mButtonNext.setEnabled(index + 1 < pageCount);
        lbl_progress = lbl_progress.replace("#",String.valueOf(index+1));
        lbl_progress = lbl_progress.replace("$",String.valueOf(pageCount));
        mLblCurrentPage.setText(lbl_progress);
        showProgress(false);
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getPageCount() {
        return mPdfRenderer.getPageCount();
    }
    public class ConsultaRutaPdfTask extends AsyncTask<Void,Void,Boolean> {
        private final String mId;
        private final String mLat;
        private final String mLong;
        private String IdRespuesta;
        ConsultaRutaPdfTask(String Id, String Lat, String Long) {
            mId = Id;
            mLat = Lat;
            mLong = Long;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                new HttpContenidos().EnviarGeoLocalizacion(mId,mLat,mLong);
            }
            catch (InterruptedException e) {
                return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        protected void onPostExecute(final Boolean success){
            mConsultarRutaPdf = null;
            showProgress(false);
            if (success) {

            }
            else {

            }
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        protected void onCancelled() {
            mConsultarRutaPdf = null;
            showProgress(false);
        }
    }
    private void receiveData() {
        Bundle bundle=getArguments();
        mContenidoId = bundle.getString("CONTENIDO_ID");
        mCursoUsuarioId = bundle.getString("CURSO_USUARIO_ID");
        mContadorPausasQuestionario = bundle.getInt("CONTADOR_PAUSAS");
        mContadorPausasQuestionario_delay = bundle.getInt("CONTADOR_PAUSAS_DELAY");
        mPaginaPausa = bundle.getString("MINUTO_PAUSA");
        mContenido =  ContenidoLab.get(getActivity(),"0").getContenido(mContenidoId);

    }
}
