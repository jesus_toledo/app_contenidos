package com.contel.android.app_contenidos;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.contel.android.app_contenidos.Entidades.Resumen;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;

import java.io.IOException;

public class HomeActivity extends AppCompatActivity {
    private static final String EXTRA_SOCIAL_SECURITY_N = "com.contel.android.app_contenidos.StrSsn";
    private static final String EXTRA_CURSO_USUARIO_ID = "com.contel.android.app_contenidos.CursoUsuarioId";
    private View mHomeFormView;
    private View mProgressView;
    private TextView mLblNombre;
    private TextView mLblNss;
    private TextView mLblCurso;
    private TextView mLblMotivo;
    private TextView mLblEstatus;
    private Button mBtnStart;
    private ObtenerResumenTask mResumenTask = null;
    private String StrSsn;
    private String CursoUsuarioId;
    private Resumen ObjResumen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        StrSsn = getIntent().getStringExtra(EXTRA_SOCIAL_SECURITY_N);
        //Asignaciones controles-variables
        mHomeFormView = findViewById(R.id.home_form);
        mProgressView = findViewById(R.id.home_progress);
        mLblNombre = findViewById(R.id.lbl_nombre);
        mLblNss = findViewById(R.id.lbl_nss);
        mLblCurso = findViewById(R.id.lbl_nombre_curso);
        mLblMotivo = findViewById(R.id.lbl_motivo);
        mLblEstatus = findViewById(R.id.lbl_estatus);
        mBtnStart = findViewById(R.id.iniciar_contenido);
        //
        if (mResumenTask != null) {
            return;
        }
        showProgress(true);
        mResumenTask = new ObtenerResumenTask(StrSsn);
        mResumenTask.execute((Void) null);
        mLblNombre.setText(StrSsn);
    }
    public static Intent newIntent(Context packageContext, String StrSsn) {
        Intent intent = new Intent(packageContext, HomeActivity.class);
        intent.putExtra(EXTRA_SOCIAL_SECURITY_N, StrSsn);
        return intent;
    }
    public void clickIniciarContenido(View view){
        if(ObjResumen==null)
            return;
        //Button botonActual = (Button) view;
        //Start ContenidosActivity
        //Intent i = new Intent(ContenidosActivity.class);
        //Intent i = ContenidoListActivity.newIntent(HomeActivity.this, CursoUsuarioId);
        //SingleFragmentActivity
        try {
            Intent intent = new Intent(HomeActivity.this, ContenidoListActivity.class);
            intent.putExtra(EXTRA_CURSO_USUARIO_ID, CursoUsuarioId);
            startActivity(intent);
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mHomeFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mBtnStart.setVisibility(show ? View.GONE : View.VISIBLE);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mHomeFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mBtnStart.setVisibility(show ? View.GONE : View.GONE);
        }
    }


    public class ObtenerResumenTask extends AsyncTask<Void,Void,Boolean>{
        private final String mSsn;
        ObtenerResumenTask(String ssn) {
            mSsn = ssn;
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            try{
                // Simulate network access.
                Thread.sleep(1);
                ObjResumen = new HttpContenidos().ResumenPeticion(mSsn);//
            }
            catch (InterruptedException e) {
            return false;
            }
            catch (Exception e){
                return false;
            }
            return true;
        }
        protected void onPostExecute(final Boolean success){
            mResumenTask = null;
            showProgress(false);
            if (success) {
                if(ObjResumen!=null){
                    mLblNombre.setText(ObjResumen.getmNombre());
                    mLblNss.setText(ObjResumen.getmNss());
                    mLblCurso.setText(ObjResumen.getmCurso());
                    mLblMotivo.setText(ObjResumen.getmMotivo());
                    mLblEstatus.setText(ObjResumen.getmEstatusCurso());
                    CursoUsuarioId = ObjResumen.getmCursoUsuarioId();
                }
            }
            else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        protected void onCancelled() {
            mResumenTask = null;
            showProgress(false);
        }
    }
}
