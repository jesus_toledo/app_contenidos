package com.contel.android.app_contenidos;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.contel.android.app_contenidos.Entidades.Contenido;

import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.getIntent;

/**
 * Created by jtoledo on 18/04/2018.
 */

public class ContenidoListFragment extends Fragment {
    private static final String TAG = "ContenidoFragment";
    private RecyclerView mCrimeRecyclerView;
    private ContenidoAdapter mAdapter;
    private String mCursoUsuarioId;
    private ContenidoLab contenidoLab;
    private static final String EXTRA_CURSO_USUARIO_ID = "com.contel.android.app_contenidos.CursoUsuarioId";
    List<Contenido> mContenidos;
    /*public ContenidoListFragment(String CursoUsuarioId){
        mCursoUsuarioId = CursoUsuarioId;
    }*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contenido_list, container, false);
        mCrimeRecyclerView = (RecyclerView) view
                .findViewById(R.id.crime_recycler_view);
        //receiveData();
        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCursoUsuarioId = (String) getActivity().getIntent()
                .getSerializableExtra(EXTRA_CURSO_USUARIO_ID);
        load_data_from_server(mCursoUsuarioId);
        return view;
    }

    private class ContenidoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Contenido mContenido;
         private TextView mTituloContenidoTextView;
         private TextView mDescripcionContenidoTextView;
        private TextView mEstatusContenidoTextView;
        private TextView mTipoContenidoTextView;
        private TextView mLblDuracionContenidoTextView;
        private TextView mDuracionContenidoTextView;
        private ImageView mContenidoIcon;
        private ContenidoLab contenidoLab;

        public ContenidoHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_contenido, parent, false));
            itemView.setOnClickListener(this);
            mTituloContenidoTextView = itemView.findViewById(R.id.nombre_fila_lista);
            mDescripcionContenidoTextView = itemView.findViewById(R.id.descripcion_fila_lista);
            mEstatusContenidoTextView = itemView.findViewById(R.id.estatus_fila_lista);
            mTipoContenidoTextView = itemView.findViewById(R.id.tipo_fila_lista);
            mDuracionContenidoTextView = itemView.findViewById(R.id.duracion_fila_lista);
            mLblDuracionContenidoTextView = itemView.findViewById(R.id.lbl_duracion_contenido);
            mContenidoIcon = itemView.findViewById(R.id.contenido_list_icon);
        }
        public void bind(Contenido contenido){

            new Thread(new Runnable() {
                public void run() {
                    try {
                        mContenido = contenido;
                        mTituloContenidoTextView.setText(mContenido.getNombre_contenido());
                        mDescripcionContenidoTextView.setText(mContenido.getDescripcion());
                        if(mContenido.getStreaming().equals("SI"))
                            mEstatusContenidoTextView.setText("STREAMING");
                        else
                            mEstatusContenidoTextView.setText("DESCARGA");
                        if(mContenido.getExtension_archivo().equals("PDF"))
                        {
                            mLblDuracionContenidoTextView.setText(getResources().getString(R.string.lbl_duracion_elemento_doc));
                            mDuracionContenidoTextView.setText(mContenido.getTotal_paginas() );
                            mContenidoIcon.setImageResource(R.drawable.if_pdf_3745);
                        }
                        else
                        {
                            mLblDuracionContenidoTextView.setText(getResources().getString(R.string.lbl_duracion_elemento_video));
                            mDuracionContenidoTextView.setText(mContenido.getTotal_duracion() + " min");
                            mContenidoIcon.setImageResource(R.drawable.if_video_173123);
                        }

                    } catch (Exception e) { // I can split the exceptions to get which error i need.
                    }
                }
            }).start();
        }

        @Override
        public void onClick(final View view) {
            //Intent intent = ContenidosActivity.newIntent(getActivity(),mContenido.getContenido_id());
            //startActivity(intent);
            Bundle bn = new Bundle();

            if(mContenido.getExtension_archivo().toUpperCase().equals("PDF")){
                bn.putString("CONTENIDO_ID", mContenido.getContenido_id());
                bn.putString("CURSO_USUARIO_ID", mCursoUsuarioId);
                bn.putInt("CONTADOR_PAUSAS", 0);
                PdfFragment fr=new PdfFragment();
                fr.setArguments(bn);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,fr)
                        .addToBackStack(null)
                        .commit();
            }
            else{
                bn.putString("CONTENIDO_ID", mContenido.getContenido_id());
                bn.putString("CURSO_USUARIO_ID", mCursoUsuarioId);
                bn.putInt("CONTADOR_PAUSAS", 0);
                ContenidoFragment fr=new ContenidoFragment();
                fr.setArguments(bn);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,fr)
                        .addToBackStack(null)
                        .commit();
            }


            /*Intent i;
            if(mContenido.getExtension_archivo().toUpperCase().equals("PDF")){
                i = new Intent(getActivity().getBaseContext(),
                        ContenidoFragment.class);
                i.putExtra("CONTENIDO_ID", mContenido.getContenido_id());
                i.putExtra("CURSO_USUARIO_ID", mCursoUsuarioId);
                i.putExtra("MINUTO_VIDEO", "0");
                //i.putExtra("CONTADOR_PAUSAS", "0");
                getActivity().startActivity(i);
            }
            else{
                i = new Intent(getActivity().getBaseContext(),
                        ContenidoFragment.class);
                i.putExtra("CONTENIDO_ID", mContenido.getContenido_id());
                i.putExtra("CURSO_USUARIO_ID", mCursoUsuarioId);
                i.putExtra("MINUTO_VIDEO", "0");
                //i.putExtra("CONTADOR_PAUSAS", "0");
                getActivity().startActivity(i);
            }*/
        }
    }
    private void updateUI() {


        //mAdapter = new HomeRecyclerAdapter(getActivity(), news_list);
        //recyclerView.setAdapter(rvAdapter);
        //mContenidos = new ArrayList<>();
            mAdapter = new ContenidoAdapter(mContenidos);
            mCrimeRecyclerView.setAdapter(mAdapter);


    }
    private class ContenidoAdapter extends RecyclerView.Adapter<ContenidoHolder> {
        private List<Contenido> mContenidos;
        public ContenidoAdapter(List<Contenido> contenidos) {
            mContenidos = contenidos;
        }

        @Override
        public ContenidoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new ContenidoHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(ContenidoHolder holder, int position) {
            Contenido contenido = mContenidos.get(position);
            holder.bind(contenido);
        }

        @Override
        public int getItemCount() {
            if(mContenidos!=null)
                return mContenidos.size();
            else
                return 0;
        }
    }
    private void load_data_from_server(String id) {
        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... integers) {
                contenidoLab = ContenidoLab.get(getActivity(),id);
                mContenidos = contenidoLab.getContenidos();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                updateUI();
            }
        };

        task.execute();
    }
    private void receiveData() {
        //RECEIVE DATA VIA INTENT
        //Intent i = getIntent();
        //mCursoUsuarioId = i.getStringExtra(EXTRA_CURSO_USUARIO_ID);
    }
}