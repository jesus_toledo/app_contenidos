package com.contel.android.app_contenidos;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.contel.android.app_contenidos.Entidades.Contenido;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;

import java.io.File;
import java.io.IOException;

import static java.lang.String.valueOf;

public class myVideoActivity extends AppCompatActivity {
    private static final String TAG = "ContenidoFragment";
    private static final String KEY_INDEX = "index";
    private static final String KEY_ARRAY_PUNTOS_PAUSA = "arreglo_puntos_pausa";
    private static final String KEY_BREAK_PONT_QUIZ_INDEX = "break_point_index";
    private static final String ARRAY_KEYS_PAUSE = "array_puntos_pausa";
    private static final String KEY_VIDEO_PROGRESS = "video_progress";
    private int mCurrentIndex = 0;
    private int mActivityOrientation;
    private Contenido mContenido;
    private TextView mTitleField;
    private TextView mDateButton;
    private Button mBtnBack;
    private static int duration = 0;
    private VideoView mVideoView;
    private int mVideoPosition = 0;
    private String mVideoPausaPosition = "0";
    private String mContadorPausasIntent = "0";
    private View mProgressView;
    private ProgressBar mProgressBar;
    private TextView mTxtVideoCurrentPos;
    private String mContenidoId;
    private String mCursoUsuarioId;
    private String uriPath;
    int mContadorPausasQuestionario = 0;
    private boolean mEntroCreate = false;
    //hilos
    private PauseVideoThread mVideoPause;
    private QuestionarioVideoThread mVideoQuestionario;
    private Handler handler = new Handler();
    private ConsultarBreakPointsTask mTaskPeticionQuiz = null;
    private String[] mArrayPuntosPausaPreguntas;
    private MediaController mediaController;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU)
            Toast.makeText(getApplicationContext(), "VIDEO EN CURSO",
                    Toast.LENGTH_LONG).show();
        return false;
        // Disable back button..............
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        setContentView(R.layout.activity_my_video);
        mEntroCreate = true;
        mVideoView = findViewById(R.id.myVideo);
        mProgressView = findViewById(R.id.video_download_progress);
        mProgressBar = findViewById(R.id.Progressbar);
        mTitleField = findViewById(R.id.crime_title);
        mTxtVideoCurrentPos = findViewById(R.id.videoCurrentPos);
        mDateButton = findViewById(R.id.crime_date);
        mActivityOrientation = getResources().getConfiguration().orientation;
        if (mActivityOrientation == Configuration.ORIENTATION_LANDSCAPE)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        else
            mTxtVideoCurrentPos.setVisibility(View.VISIBLE);
        Log.i(TAG, "metodo on create llamado");
        receiveData();
        mContenido = ContenidoLab.get(this, "0").getContenido(mContenidoId);
        mTitleField.setText(mContenido.getNombre_contenido());
        mDateButton.setText(mContenido.getEstatus());
        //Video

        if (savedInstanceState != null) {
            Log.i(TAG, "si hay instancia guardada");
            //mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mVideoPosition = savedInstanceState.getInt(KEY_VIDEO_PROGRESS);
            mContadorPausasQuestionario = savedInstanceState.getInt(KEY_BREAK_PONT_QUIZ_INDEX);
            mArrayPuntosPausaPreguntas = savedInstanceState.getStringArray(ARRAY_KEYS_PAUSE);
            //current = mVideoPosition;
            Log.i(TAG, "segundo -> " + String.valueOf(mVideoPosition));
        } else {
            //mVideoPosition = 0;
            mTaskPeticionQuiz = new ConsultarBreakPointsTask(mContenidoId);
            mTaskPeticionQuiz.execute((Void) null);
        }
        if (mContenido.getStreaming().equals("NO"))
            Descargar_Contenido();
        else
            setUpVideoView();
    }

    private void Descargar_Contenido() {
        String[] Partes_Url;
        String url = mContenido.getUrl_acceso();

        String externalStroageString = String.valueOf(getFilesDir());//+"/Pdf/";
        Partes_Url = url.split("/");
        String fileName = "/" + Partes_Url[Partes_Url.length - 1];
        //File file = new File(externalStroageString);
        File file = new File(Environment.getExternalStoragePublicDirectory(String.valueOf(getFilesDir() + "/")), fileName);
        uriPath = file.getPath();

        File dir = new File(externalStroageString);
        if (dir.exists() == false) {
            dir.mkdirs();
        }
        if (file.isFile() == false) {
            //showProgress(true);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            request.setDescription(fileName);
            request.setTitle("Contenidos App");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(externalStroageString, fileName); //Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        } else {
            try {
                setUpVideoView();
                //openRenderer(this);
                //showPage(mPageIndex);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Toast.makeText(myVideoActivity.this, "Se descargó Video", Toast.LENGTH_SHORT).show();
            try {
                showProgress(false);
                //uriPath = file.getPath();
                setUpVideoView();
                //openRenderer(myPfdActivity.this);
                //showPage(mPageIndex);

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(myVideoActivity.this, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    private void setUpVideoView() {
        // Prepara la URI del vídeo que será reproducido.
        //String uriPath = Environment.getExternalStorageDirectory()+"/Movies/"
        //+ "salvar una relacion.mp4";
        if (mContenido.getStreaming().equals("SI"))
            uriPath = mContenido.getUrl_acceso();
        Uri uri = Uri.parse(uriPath);
        // Se crean los controles multimedia.
        mediaController = new MediaController(this);
        // Asigna los controles multimedia a la VideoView.
        mVideoView.setMediaController(mediaController);
        try {
            // Asigna la URI del vídeo que será reproducido a la vista.
            mVideoView.setVideoPath(uriPath);
            // Se asigna el foco a la VideoView.
            mVideoView.requestFocus();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        /*
         * Se asigna un listener que nos informa cuando el vídeo
         * está listo para ser reproducido.
         */
        mVideoView.setOnPreparedListener(videoViewListener);
    }

    private MediaPlayer.OnPreparedListener videoViewListener = new MediaPlayer.OnPreparedListener() {
        int posicion_video = 0;

        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            /*
             * Se indica al reproductor multimedia que el vídeo
             * se reproducirá en un loop (on repeat).
             */
            duration = mVideoView.getDuration();
            if(mVideoPausaPosition.equals("0"))
                mVideoView.seekTo(mVideoPosition);
            else
                mVideoView.seekTo(Integer.parseInt(mVideoPausaPosition));
            mVideoView.start();

            //Si viene de actividad cuestionario se regresa al punto anterior
            /*mVideoPause = new PauseVideoThread();
            final Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    if ( mArrayPuntosPausaPreguntas != null) {
                        if (mContadorPausasQuestionario < mArrayPuntosPausaPreguntas.length) {
                            if (mContadorPausasQuestionario > 0)
                                mVideoView.seekTo(Integer.parseInt(mArrayPuntosPausaPreguntas[mContadorPausasQuestionario - 1]));
                            mVideoView.start();
                        }
                    }
                }
        };
            mVideoPause.start();
            mVideoPause.prepareHandler();
            mVideoPause.postTask(myRunnable);*/
            //mTaskProgresoVideo = new MyAsync();
            //mTaskProgresoVideo.execute((Void) null);

            /*mVideoPause = new PauseVideoThread();
            final Runnable myRunnable = new Runnable() {
                int minuto_pausa = 0;
                boolean bandera = true;
                @Override
                public void run() {
                    do{
                        try{
                            if(mArrayPuntosPausaPreguntas!=null){
                                if(mContadorPausasQuestionario<mArrayPuntosPausaPreguntas.length) {
                                    minuto_pausa = Integer.parseInt(mArrayPuntosPausaPreguntas[mContadorPausasQuestionario]);
                                    if (current >= minuto_pausa) {
                                        bandera = false;
                                        mVideoView.pause();
                                        mContadorPausasQuestionario++;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.i(TAG,"error en el pausado del video: " + e.getMessage());
                        }
                    }
                    while(bandera);
                }
            };
            mVideoPause.start();
            mVideoPause.prepareHandler();
            mVideoPause.postTask(myRunnable);*/

            mVideoQuestionario = new QuestionarioVideoThread();
            final Runnable myRunnable2 = new Runnable() {
                int minuto_pausa = 0;
                boolean bandera = true;

                @Override
                public void run() {
                    do {
                        try {
                            if (mContadorPausasQuestionario < mArrayPuntosPausaPreguntas.length) {
                                minuto_pausa = Integer.parseInt(mArrayPuntosPausaPreguntas[mContadorPausasQuestionario]);
                                if (mVideoView.getCurrentPosition() >= minuto_pausa && mVideoView.getCurrentPosition() > 0 && mVideoView.getCurrentPosition() < 20000000) {

                                    mContadorPausasQuestionario++;
                                    Log.i(TAG, "current: " + mVideoView.getCurrentPosition() + " minuto_pausa: " + minuto_pausa);
                                    mVideoView.pause();
                                    bandera = false;
                                    //mTaskProgresoVideo.cancel(true);
                                    //mVideoPause.quit();
                                    //mVideoQuestionario.quit();
                                    //= null;
                                    Intent i = new Intent(getBaseContext(),
                                            QuestionarioActivity.class);
                                    i.putExtra("OTRO_CONTENIDO_ID", mContenido.getContenido_id());
                                    i.putExtra("CURSO_USUARIO_ID", mCursoUsuarioId);
                                    i.putExtra("MINUTO_PAUSA", String.valueOf(minuto_pausa));
                                    //i.putExtra("CONTADOR_PAUSAS", String.valueOf(mContadorPausasQuestionario));
                                    //START ACTIVITY
                                    //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    //myVideoActivity.this.finish();
                                }
                            }
                        } catch (Exception ex) {
                            Log.i(TAG, "hi im archie " + ex.getMessage());
                        }

                    }
                    while (bandera);
                }
            };
            mVideoQuestionario.start();
            mVideoQuestionario.prepareHandler();
            mVideoQuestionario.postTask(myRunnable2);
        }
    };

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "se guardo video en posicion: " + valueOf(mVideoView.getCurrentPosition()));
        savedInstanceState.putInt(KEY_INDEX, mVideoView.getCurrentPosition());
        savedInstanceState.putInt(KEY_VIDEO_PROGRESS, mVideoView.getCurrentPosition());
        savedInstanceState.putInt(KEY_BREAK_PONT_QUIZ_INDEX, mContadorPausasQuestionario);
        savedInstanceState.putStringArray(ARRAY_KEYS_PAUSE, mArrayPuntosPausaPreguntas);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "metodo on start llamado");
        super.onStart();

        if (!mEntroCreate) {
            //finish();
            //super.onDestroy();

            //mVideoQuestionario.interrupt();
            /*if(mVideoPause!=null)
                mVideoPause.quit();
            if(mVideoQuestionario!=null)
                mVideoQuestionario.quit();
            finish();*/
            //finish();
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "metodo on resume llamado");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "metodo on pause llamado");
        super.onPause();

    }

    @Override
    protected void onStop() {
        //mVideoView.stopPlayback();
        if (mVideoQuestionario!= null)
            mVideoQuestionario.interrupt();
        if (mVideoPause!= null)
        mVideoPause.interrupt();
        /*if (mVideoQuestionario!= null) {
            Thread moribund = mVideoQuestionario;
            mVideoQuestionario = null;
            moribund.interrupt();
        }
        if (mVideoPause!= null) {
            Thread moribund = mVideoPause;
            mVideoPause = null;
            moribund.interrupt();
        }
        if (!mEntroCreate) {
            Log.d(TAG, "se termina actividad " + mEntroCreate);
            if(mVideoPause!=null)
                mVideoPause.quit();
            if(mVideoQuestionario!=null)
                mVideoQuestionario.quit();
            finish();}
        mEntroCreate = false;*/
        super.onStop();

        //mTaskProgresoVideo = null;
        //mVideoProgresoPausa = null;
        //if (mVideoQuestionario != null){
            //mVideoQuestionario.interrupt();
            //mVideoQuestionario.quit();
        //}
        //mTaskProgresoVideo.cancel(true);
        //mTaskProgresoVideo = null;
        Log.d(TAG, "metodo on stop llamado");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "metodo on destroy llamado");
    }

    private void VideoPlayerFeatures() {
        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(mVideoView);
        mVideoView.setMediaController(vidControl);
    }

    private class GetVideoProgress extends HandlerThread {
        private Handler handler;
        private static final String TAG = "Getting current position";

        public GetVideoProgress() {
            super(TAG);
        }

        public void postTask(Runnable task) {
            handler.post(task);
        }

        public void prepareHandler() {
            handler = new Handler(getLooper());
        }
    }
    public void Btn_Regresar_Click(View view){
        if(mVideoPause!=null)
            mVideoPause.quit();
        if(mVideoQuestionario!=null)
            mVideoQuestionario.quit();
        //finish();
    }
    private void receiveData() {
        //RECEIVE DATA VIA INTENT
        Intent i = getIntent();
        mContenidoId = i.getStringExtra("CONTENIDO_ID");
        mCursoUsuarioId = i.getStringExtra("CURSO_USUARIO_ID");
        mVideoPausaPosition = i.getStringExtra("MINUTO_VIDEO");
        //mContadorPausasIntent = i.getStringExtra("CONTADOR_PAUSAS");
        //mVideoPosition = i.getIntExtra("MINUTO_VIDEO",0);
        //mContadorPausasQuestionario = i.getIntExtra("CONTADOR_PAUSAS",0);
    }


    public class ConsultarBreakPointsTask extends AsyncTask<Void, Void, Boolean> {
        Boolean blnAcceso = false;
        private final String mContid;
        //private final String mPassword;

        ConsultarBreakPointsTask(String contenidoId) {
            mContid = contenidoId;
            //mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(1);
                mArrayPuntosPausaPreguntas = new HttpContenidos().PasuaPreguntasPeticion(mContid);//
            } catch (InterruptedException e) {
                return false;
            }
            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mTaskPeticionQuiz = null;
            if (success) {
            }
        }

        @Override
        protected void onCancelled() {
            mTaskPeticionQuiz = null;

        }
    }

    private class PauseVideoThread extends HandlerThread {
        private Handler handler;
        private static final String TAG = "Pausa video";

        public PauseVideoThread() {
            super(TAG);
        }

        public void postTask(Runnable task) {
            handler.post(task);
        }

        public void prepareHandler() {
            handler = new Handler(getLooper());
        }
    }

    private class QuestionarioVideoThread extends HandlerThread {
        private Handler handler;
        private static final String TAG = "Pausa video";

        public QuestionarioVideoThread() {
            super(TAG);
        }

        public void postTask(Runnable task) {
            handler.post(task);
        }

        public void prepareHandler() {
            handler = new Handler(getLooper());
        }
    }
}
