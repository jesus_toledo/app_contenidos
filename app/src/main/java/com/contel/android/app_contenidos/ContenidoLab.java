package com.contel.android.app_contenidos;

import android.content.Context;

import com.contel.android.app_contenidos.Entidades.Contenido;
import com.contel.android.app_contenidos.Peticiones.HttpContenidos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jtoledo on 18/04/2018.
 */

public class ContenidoLab {
    private static String mCursoUsuarioId;
    private static ContenidoLab sContenidoLab;
    private List<Contenido> mContenidos;

    public static ContenidoLab get(Context context, String Curso_Id) {
        if (sContenidoLab == null) {
            sContenidoLab = new ContenidoLab(context,Curso_Id);
            //sContenidoLab = new ContenidoLab(context);
        }
        return sContenidoLab;
    }
    private ContenidoLab(Context context, String Curso_Id) {
        mContenidos = new ArrayList();
        if(Curso_Id!="0")
            mContenidos = new HttpContenidos().ObtenerContenidos(Curso_Id);
    }
    public List<Contenido> getContenidos() {
        return mContenidos;
    }
    public Contenido getContenido(String id) {
        for (Contenido contenido : mContenidos) {
            if (contenido.getContenido_id().equals(id)) {
                return contenido;
            }
        }
        return null;
    }


}
