package com.contel.android.app_contenidos.Utils;

import android.content.Context;

import com.contel.android.app_contenidos.Entidades.Resumen;

/**
 * Created by jtoledo on 13/05/2018.
 */

public class St_Curso_Datos {
    private static St_Curso_Datos s_St_Curso_Datos;
    private Resumen mResumen;
    public static St_Curso_Datos get(Context context){
        if(s_St_Curso_Datos == null){
            s_St_Curso_Datos = new St_Curso_Datos(context);
        }
        return s_St_Curso_Datos;
    }
    private St_Curso_Datos(Context context){
        mResumen = new Resumen();
    }
    public Resumen getResumen() {
        return mResumen;
    }

}
