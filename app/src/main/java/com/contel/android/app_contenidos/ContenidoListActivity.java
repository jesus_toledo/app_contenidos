package com.contel.android.app_contenidos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import static android.app.PendingIntent.getActivity;
import static android.content.Intent.getIntent;

/**
 * Created by jtoledo on 18/04/2018.
 */

public class ContenidoListActivity extends SingleFragmentActivity{
    private static final String EXTRA_CURSO_USUARIO_ID = "com.contel.android.app_contenidos.CursoUsuarioId";
    private String mCursoUsuarioId = "0";

    @Override
    protected Fragment createFragment() {
        return new ContenidoListFragment();
    }
    public static Intent newIntent(Context packageContext, String cursoUsuarioId) {
        Intent intent = new Intent(packageContext, HomeActivity.class);
        intent.putExtra(EXTRA_CURSO_USUARIO_ID, cursoUsuarioId);
        return intent;
    }
    private void receiveData() {
        //RECEIVE DATA VIA INTENT
        Intent i = getIntent();
        mCursoUsuarioId = i.getStringExtra(EXTRA_CURSO_USUARIO_ID);
    }
    private void sendData(){

    }
}
