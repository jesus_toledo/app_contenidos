package com.contel.android.app_contenidos.Entidades;

/**
 * Created by jtoledo on 26/04/2018.
 */

public class Respuestas {
    private String pregunta_id;
    private String respuesta_id;
    private String nombre_respuesta;
    private String orden;
    private String correcta;

    public String getPregunta_id() {
        return pregunta_id;
    }

    public void setPregunta_id(String pregunta_id) {
        this.pregunta_id = pregunta_id;
    }

    public String getRespuesta_id() {
        return respuesta_id;
    }

    public void setRespuesta_id(String respuesta_id) {
        this.respuesta_id = respuesta_id;
    }

    public String getNombre_respuesta() {
        return nombre_respuesta;
    }

    public void setNombre_respuesta(String nombre_respuesta) {
        this.nombre_respuesta = nombre_respuesta;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getCorrecta() {
        return correcta;
    }

    public void setCorrecta(String correcta) {
        this.correcta = correcta;
    }
}
