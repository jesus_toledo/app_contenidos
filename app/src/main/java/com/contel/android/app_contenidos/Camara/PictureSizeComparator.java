package com.contel.android.app_contenidos.Camara;

import android.hardware.Camera;

import java.util.Comparator;

/**
 * Created by jtoledo on 10/05/2018.
 */

class PictureSizeComparator implements Comparator<Camera.Size> {
    // Used for sorting in ascending order of
    // roll name
    public int compare(Camera.Size a, Camera.Size b) {
        return (b.height * b.width) - (a.height * a.width);
    }
}
