package com.contel.android.app_contenidos.Entidades;

/**
 * Created by jtoledo on 25/04/2018.
 */

public class Cuestionario {
    private String pregunta_id;
    private String respuesta_id;
    private String nombre_pregunta;

    public String getPregunta_id() {
        return pregunta_id;
    }

    public void setPregunta_id(String pregunta_id) {
        this.pregunta_id = pregunta_id;
    }

    public String getRespuesta_id() {
        return respuesta_id;
    }

    public void setRespuesta_id(String respuesta_id) {
        this.respuesta_id = respuesta_id;
    }

    public String getNombre_pregunta() {
        return nombre_pregunta;
    }

    public void setNombre_pregunta(String nombre_pregunta) {
        this.nombre_pregunta = nombre_pregunta;
    }
}
