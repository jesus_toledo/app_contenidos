package com.contel.android.app_contenidos.Entidades;

/**
 * Created by jtoledo on 17/04/2018.
 */

public class Resumen {
    private String mmotivo;
    private String mnombre;
    private String mnss;
    private String mcurso;
    private String mestatuscurso;
    private String mcursousuarioid;

    public String getmMotivo() {
        return mmotivo;
    }

    public void setmMotivo(String mMotivo) {
        this.mmotivo = mMotivo;
    }

    public String getmNombre() {
        return mnombre;
    }

    public void setmNombre(String mNombre) {
        this.mnombre = mNombre;
    }

    public String getmCurso() {
        return mcurso;
    }

    public void setmCurso(String mCurso) {
        this.mcurso = mCurso;
    }

    public String getmEstatusCurso() {
        return mestatuscurso;
    }

    public void setmEstatusCurso(String mEstatusCurso) {
        this.mestatuscurso = mEstatusCurso;
    }

    public String getmNss() {
        return mnss;
    }

    public void setmNss(String mNss) {
        this.mnss = mNss;
    }

    public String getmCursoUsuarioId() {
        return mcursousuarioid;
    }

    public void setmCursoUsuarioId(String mCursoUsuarioId) {
        this.mcursousuarioid = mCursoUsuarioId;
    }
}
